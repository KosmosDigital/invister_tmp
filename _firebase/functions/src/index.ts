import * as admin from "firebase-admin";

admin.initializeApp();

export {
  addToIndex,
  updateIndex,
  deleteFromIndex,
  addNotification,
  deletePlayer,
  deleteUser,
  updatePlayerEvaluation,
  deletePlayerMethodology,
  createNewMethodology,
} from "./firestore";
export { approveUser, addUser } from "./callable";
