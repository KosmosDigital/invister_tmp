import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import algoliasearch from "algoliasearch";

const firestore = admin.firestore();

/*
 *  ALGOLIA
 */

const APP_ID = functions.config().algolia.app;
const ADMIN_KEY = functions.config().algolia.key;

const client = algoliasearch(APP_ID, ADMIN_KEY);
const index = client.initIndex("players");
index.setSettings({ attributesForFaceting: ["owner"] });

export const addToIndex = functions.firestore
  .document("players/{playerId}")
  .onCreate(async (snapshot) => {
    const objectID = snapshot.id;
    const data = snapshot.data();
    data.owner = data.owner.split("_"); // split the owner (uid1_uid2_uid3) into an array for filtering purposes

    return index.saveObject({ ...data, objectID });
  });

export const updateIndex = functions.firestore
  .document("players/{playerId}")
  .onUpdate(async (change, context) => {
    const newData = change.after.data();
    const oldData = change.before.data();

    if (newData.flutterUpdate && newData.position !== oldData.position) {
      await firestore
        .collection("players")
        .doc(context.params.playerId)
        .update({ grade: null, flutterUpdate: false });
    }
    newData.owner = newData.owner.split("_");
    const objectID = change.after.id;

    // add methodology name for a better search
    if (newData.position) {
      const methodologySnapshot = await firestore
        .collection("methodologies")
        .doc(newData.position)
        .get();
      const methodology = methodologySnapshot.data();
      if (methodology !== undefined) newData.positionName = methodology.name;
    }

    // add sex name
    if (newData.sex === "male") newData.sexName = "homme";
    if (newData.sex === "female") newData.sexName = "femme";

    // add date
    if (newData.dateOfBirth)
      newData.dateOfBirthString = newData.dateOfBirth.toDate().toString();

    return index.saveObject({ ...newData, objectID });
  });

export const deleteFromIndex = functions.firestore
  .document("players/{playerId}")

  .onDelete((snapshot) => index.deleteObject(snapshot.id));

/*
 * Notifications on new player
 */

export const addNotification = functions.firestore
  .document("players/{playerId}")
  .onCreate(async (snapshot, context) => {
    const data = snapshot.data();
    const owners: string[] = data.owner.split("_"); // split the owner (uid1_uid2_uid3) into an array

    owners.pop(); // remove the last element of the array, as it's supposed to be the player's creator so no need for notifications

    for await (const owner of owners) {
      const userSnapshot = await firestore.collection("users").doc(owner).get();
      const user = userSnapshot.data();
      if (user === undefined) return;
      if (!user.notifications) user.notifications = {};
      if (!user.notifications.newPlayers) user.notifications.newPlayers = [];
      user.notifications.newPlayers.push(context.params.playerId);
      await firestore
        .collection("users")
        .doc(owner)
        .update({ notifications: user.notifications });
    }

    return;
  });

/*
 * Clean player deletion
 */

export const deletePlayer = functions.firestore
  .document("players/{playerId}")
  .onDelete(async (snapshot, context) => {
    const playerId = context.params.playerId;

    // TODO check if the caller is the rightfull owner of the deleted player
    //const callerUid = context.auth.uid;

    await firestore
      .collection("players_classifications")
      .doc(playerId)
      .delete();

    // TODO delete user's notifications as well

    const docs = await firestore
      .collection("users")
      .where("notifications.newPlayers", "array-contains", playerId)
      .get();

    docs.forEach(async (doc) => {
      const data = doc.data();
      const remainingPlayers = data.notifications.newPlayers.filter(
        (player: string) => player !== playerId
      );
      data.notifications.newPlayers = remainingPlayers;

      await firestore
        .collection("users")
        .doc(doc.id)
        .update({ notifications: data.notifications });
    });

    return;
  });

/*
 * Clean user deletion
 */

export const deleteUser = functions.firestore
  .document("users/{userId}")
  .onDelete((snapshot, context) => {
    const userId = context.params.userId;
    return admin.auth().deleteUser(userId);
  });

/*
 * Update player evaluation
 */
export const updatePlayerEvaluation = functions.firestore
  .document("players/{playerUid}/evaluations/{playerEvaluationUid}")
  .onUpdate(async (change, context) => {
    const playerId = context.params.playerUid;

    const newData = change.after.data();
    const oldData = change.before.data();
    let grade: number | null = null;
    let playerGrade: number = -1;
    let i = 0;

    if (newData.flutterUpdate) {
      newData.categories.forEach((cat: any) => {
        const tmpcat = oldData.categories.find(
          (value: any) => value.type === cat.type
        );
        if (cat.type !== newData.lastCatUpdate) {
          cat.items = tmpcat.items;
        } else {
          cat.items.forEach((item: any) => {
            const tmpitem = tmpcat.items.find(
              (value: any) => value.title === item.title
            );
            if (
              item.title !== newData.lastItemUpdate &&
              tmpitem !== undefined
            ) {
              item.criterias = tmpitem.criterias;
            }
          });
        }
      });

      grade = getAverage(newData);

      await firestore
        .collection("players/"+playerId+"/evaluations/")
        .doc(context.params.playerEvaluationUid)
        .update({ ...newData, flutterUpdate: false, grade: grade });

      const evaluations = (await firestore
        .collection("players/" + playerId + "/evaluations")
        .listDocuments()).map((evaluation) => {
          return evaluation.get()
        });

      const evalData = await Promise.all(evaluations);
      evalData.forEach((val: any) => {
        const tmpData = val.data();
        const tmpGrade = getAverage(tmpData);
 
        if (tmpGrade !== null && playerGrade !== -1) {
          playerGrade += tmpGrade;
          i++;
        } else if (tmpGrade !== null && playerGrade === -1) {
          playerGrade = tmpGrade;
          i++;
        }
      });

      if (grade !== null && playerGrade === -1) {
        i++;
        playerGrade = grade;
      }

      if (i > 0 && playerGrade !== -1) {
        playerGrade = playerGrade / i;
      }

      if (playerGrade !== -1) {
        await firestore
          .collection("players")
          .doc(context.params.playerUid)
          .update({ grade: Math.round((playerGrade + Number.EPSILON) * 100) / 100 });
      }
    }

    return;
  });

  function getAverage(newData: FirebaseFirestore.DocumentData): number | null {
    let grade: number | null = null;
    let i = 0;

    newData.categories.forEach((cat: any) => {
      cat.items.forEach((item: any) => {
        if (item !== undefined) {
          item.criterias.forEach((crit: any) => {
            if (grade === null || grade === undefined) {
              grade = 0.0;
            }
            if (crit.grade !== null || crit.grade !== undefined) {
              i++;
              grade = grade + crit.grade;
            }
          });
        }
      });
    });

    if (i > 0 && grade !== null) {
      grade = grade / i;
    }

    return grade;
  }

/*
 * Delete player's classifications and evaluations
 */

export const deletePlayerMethodology = functions.firestore
  .document("players/{playerId}")
  .onUpdate(async (change, context) => {
    const playerId = context.params.playerId;

    const oldPlayer = change.before.data();
    const newPlayer = change.after.data();

    if (
      oldPlayer.position &&
      oldPlayer.position !== "" &&
      newPlayer.position &&
      newPlayer.position !== oldPlayer.position
    ) {
      await firestore
        .collection("players_classifications")
        .doc(playerId)
        .delete();
      await firestore
        .collection(`players/${playerId}/evaluations`)
        .listDocuments().then((doc) => {
          doc.map(async (val) => {
            await val.delete();
          });
        });
    }

    return;
  });

/*
 * Delete player's classifications and evaluations
 */

export const createNewMethodology = functions.firestore
  .document("methodologies/{methodologyId}")
  .onUpdate(async (change, context) => {
    const methodologyId = context.params.methodologyId;

    const oldMetodology = change.before.data();
    const newMethodology = change.after.data();

    // avoid callback loop
    if (newMethodology.owner === "") return;

    oldMetodology.owner = "";

    await firestore.collection("methodologies").add(newMethodology);

    await firestore
      .collection("methodologies")
      .doc(methodologyId)
      .set(oldMetodology);

    return;
  });
