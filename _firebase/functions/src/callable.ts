import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

const firestore = admin.firestore();
const auth = admin.auth();

/* Set admin, DO NOT UNCOMMENT IT unless you know what you're doing dude*/

// export const setAdmin = functions.https.onCall((data, context) => {
//   return auth.setCustomUserClaims(data.uid, { admin: true });
// });

export const approveUser = functions.https.onCall(async (data, context) => {
  try {
    if (!context.auth) {
      return;
    }

    const callerUid = context.auth.uid;
    const callerUserRecord = await auth.getUser(callerUid);
    if (
      !callerUserRecord.customClaims ||
      !callerUserRecord.customClaims.admin
    ) {
      return;
    }

    const userId = data.userId;

    // Enable user both on the auth and firestore side
    await auth.updateUser(userId, { disabled: false });
    await firestore.collection("users").doc(userId).update({ approved: true });

    // Send an email to the HSX
    const doc = await firestore.collection("users").doc(userId).get();
    const user = doc.data();
    if (!user) throw new Error("Unknown user");
    const hsxId = user.parent.split("_")[0];
    const authHsx = await auth.getUser(hsxId);
    const authUser = await auth.getUser(userId);

    await firestore.collection("mail").add({
      to: authHsx.email,
      message: {
        subject: "[Invister] Compte validé",
        text: `L'utilisateur ${user.firstName} ${user.lastName} (${authUser.email}) a été validé!`,
      },
    });

    await firestore.collection("mail").add({
      to: authUser.email,
      message: {
        subject: "[Invister] Compte validé",
        text: `Félicitations, votre compte Invister (${authUser.email}) a été créé! Par sécurité nous n'envoyons pas le mot de passe par mail. Veuillez contacter votre Head Scout X pour l'obtenir.`,
      },
    });

    return;
  } catch (e) {
    console.log(e);
    return;
  }
});

export const addUser = functions.https.onCall(async (data, context) => {
  try {
    if (!context.auth) {
      return;
    }

    const hsxUserDoc = await firestore
      .collection("users")
      .doc(context.auth.uid)
      .get();

    const hsxUser = hsxUserDoc.data();
    if (!hsxUser) return;

    const userRecord = await auth.createUser({
      email: data.email,
      password: data.password,
      disabled: true,
    });

    await firestore.collection("mail").add({
      to: "audric.manaud@gmail.com",
      message: {
        subject: "[Invister] Nouvelle demande de validation",
        text: `Une demande de validation a été émise pour l'utilisateur ${data.firstName} ${data.lastName}.`,
      },
    });

    return firestore
      .collection("users")
      .doc(userRecord.uid)
      .set({
        firstName: data.firstName,
        lastName: data.lastName,
        parent: data.parent,
        approved: false,
        isNotified: true,
        ...(hsxUser.club && { club: hsxUser.club }),
      });
  } catch (e) {
    console.log(e);
    return;
  }
});
