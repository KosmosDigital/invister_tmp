import React, { useState } from "react";
import "./ResetPasswordPage.css";
import {
  Button,
  Typography,
  Snackbar,
} from "@material-ui/core";
import CustomTextField from "../../components/CustomTextField/CustomTextField";
import firebase from "firebase";
import { Link } from "react-router-dom";
import { Formik, ErrorMessage } from "formik";
import { Alert } from "@material-ui/lab";
import { useTranslation } from "react-i18next";
import { CgvuLink } from "../../components/Cgvu/CgvuLink";


const ResetPasswordPage = () => {
    const { t } = useTranslation();

  const auth = firebase.auth();

  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");

  const [openSuccess, setOpenSuccess] = useState(false);
  const [success, setSuccess] = useState("");

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleCloseSuccess = (
    event?: React.SyntheticEvent,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSuccess(false);
  };

  return (
    <React.Fragment>
      <div className="mt-8">

        <div
          className="d-flex flex-column align-items-sm-end align-items-center"
          style={{ marginTop: "13%" }}
          id="forgotPassword_formContainer"
        >
          <Formik
            initialValues={{
              email: "",
            }}
            validate={(values) => {
              const errors: any = {};
              if (!values.email) {
                errors.email = "Champs obligatoire";
              }
              return errors;
            }}
            onSubmit={async (values, { setSubmitting }) => {
              auth
                .sendPasswordResetEmail(values.email)
                .then((user) => {
                  setSuccess("Un e-mail de réinitialisation a été envoyé!");
                  setOpenSuccess(true);
                })
                .catch((error) => {
                  setError(error.message);
                  setOpen(true);
                });
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <form onSubmit={handleSubmit}>
                <div className="d-flex flex-column">
                  <Typography variant="h5" color="textPrimary">
                    Entrez votre e-mail
                  </Typography>

                  <CustomTextField
                    label="E-mail*"
                    type="text"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    disabled={isSubmitting}
                    value={values.email}
                    className="mt-3"
                  />
                  <ErrorMessage
                    name="email"
                    render={(msg) => (
                      <span style={{ color: "red" }}>{msg}</span>
                    )}
                  />
                </div>
                <Button
                  type="submit"
                  variant="contained"
                  style={{
                    background: "linear-gradient(#01B3F4, #044CAB)",
                    color: "white",
                    borderRadius: "30px",
                  }}
                  className="mt-4"
                  size="large"
                >
                  <div className="mx-3">
                    {t("forgotPassword.reinitPass")}
                  </div>
                </Button>
                <Link
                  to="/signup"
                  style={{ color: "white", textDecoration: "none" }}
                  className="d-block d-sm-none mt-4"
                >
                  <p>{t("forgotPassword.noaccount")}</p>
                </Link>
              </form>
            )}
          </Formik>
          <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              {error}
            </Alert>
          </Snackbar>
          <Snackbar
            open={openSuccess}
            autoHideDuration={6000}
            onClose={handleCloseSuccess}
          >
            <Alert onClose={handleCloseSuccess} severity="success">
              {success}
            </Alert>
          </Snackbar>
        </div>
      </div>
      <CgvuLink />
    </React.Fragment>
  );
};
export default ResetPasswordPage;
