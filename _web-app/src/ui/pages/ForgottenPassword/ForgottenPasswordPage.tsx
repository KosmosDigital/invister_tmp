import React, { useState } from "react";
import "./ForgottenPasswordPage.css";
import {
  Button,
  Typography,
  Snackbar,
} from "@material-ui/core";
import CustomTextField from "../../components/CustomTextField/CustomTextField";
import firebase from "firebase";
import { Link } from "react-router-dom";
import { Formik, ErrorMessage } from "formik";
import { Alert } from "@material-ui/lab";
import { ReactComponent as Logo } from "../../../assets/logo_invister.svg";
import whiteLogo from "../../../assets/logo_invister_blanc.svg";
import FlagIcon from "../../components/FlagButton/FlagIcon";
import { useTranslation } from "react-i18next";
import { CgvuLink } from "../../components/Cgvu/CgvuLink";


const ForgottenPasswordPage = () => {
  const { t } = useTranslation();

  const auth = firebase.auth();

  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");

  const [openSuccess, setOpenSuccess] = useState(false);
  const [success, setSuccess] = useState("");

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleCloseSuccess = (
    event?: React.SyntheticEvent,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSuccess(false);
  };

  return (
    <React.Fragment>
      <img src="/landingpage.png" id="bg" alt=""></img>
      <img src="/landingpage_mobile.png" id="bg_mobile" alt=""></img>
      <div className="mt-8">
        <div className="d-flex flex-row-reverse pt-2 pr-2 pt-sm-4 pr-sm-4 justify-content-between justify-content-sm-start">
          <FlagIcon />
          <Link
            to="/login"
            style={{ color: "white", textDecoration: "none" }}
            className="d-sm-block d-none"
          >
            <Button
              className="ml-2 my-2"
              variant="outlined"
              style={{
                color: "white",
                textTransform: "capitalize",
                border: "1px solid rgba(255, 255, 255, 1)",
                borderRadius: "30px",
              }}
            >
              <div className="mx-3">{t("forgotPassword.Se_connecter")}</div>
            </Button>
          </Link>
          <Button
            style={{
              color: "white",
              textTransform: "capitalize",
              textDecoration: "underline",
            }}
            className="d-sm-block d-none"
          >
            {t("forgotPassword.Contactez-nous")}
          </Button>
          <img
            src={whiteLogo}
            alt=""
            width="170"
            className="d-block d-sm-none pl-4"
          ></img>
        </div>

        <div className="d-flex flex-column">
          <Logo
            style={{
              top: "-3em",
              marginTop: "-1.4rem",
              marginLeft: "-7rem",
            }}
            width="50rem"
            height="5rem"
            className="d-sm-block d-none"
          />

          <p
            className="text-left font-weight-bold d-sm-block d-none"
            style={{
              fontSize: "13px",
              top: "-3em",
              marginTop: "-1.4rem",
              marginLeft: "11.9rem",
            }}
          >
            working for tomorrow
          </p>
        </div>

        <div
          className="d-flex flex-column align-items-sm-end align-items-center"
          style={{ marginTop: "13%" }}
          id="forgotPassword_formContainer"
        >
          <Formik
            initialValues={{
              email: "",
            }}
            validate={(values) => {
              const errors: any = {};
              if (!values.email) {
                errors.email = "Champs obligatoire";
              }
              return errors;
            }}
            onSubmit={async (values, { setSubmitting }) => {
              auth
                .sendPasswordResetEmail(values.email)
                .then((user) => {
                  setSuccess("Un e-mail de réinitialisation a été envoyé!");
                  setOpenSuccess(true);
                })
                .catch((error) => {
                  setError(error.message);
                  setOpen(true);
                });
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <form onSubmit={handleSubmit}>
                <div className="d-flex flex-column">
                  <Typography variant="h5" color="textPrimary">
                    Entrez votre e-mail
                  </Typography>

                  <CustomTextField
                    label="E-mail*"
                    type="text"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    disabled={isSubmitting}
                    value={values.email}
                    className="mt-3"
                  />
                  <ErrorMessage
                    name="email"
                    render={(msg) => (
                      <span style={{ color: "red" }}>{msg}</span>
                    )}
                  />
                </div>
                <Button
                  type="submit"
                  variant="contained"
                  style={{
                    background: "linear-gradient(#01B3F4, #044CAB)",
                    color: "white",
                    borderRadius: "30px",
                  }}
                  className="mt-4"
                  size="large"
                >
                  <div className="mx-3">
                    {t("forgotPassword.reinitPass")}
                  </div>
                </Button>
                <Link
                  to="/signup"
                  style={{ color: "white", textDecoration: "none" }}
                  className="d-block d-sm-none mt-4"
                >
                  <p>{t("forgotPassword.noaccount")}</p>
                </Link>
              </form>
            )}
          </Formik>
          <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              {error}
            </Alert>
          </Snackbar>
          <Snackbar
            open={openSuccess}
            autoHideDuration={6000}
            onClose={handleCloseSuccess}
          >
            <Alert onClose={handleCloseSuccess} severity="success">
              {success}
            </Alert>
          </Snackbar>
        </div>
      </div>
      <CgvuLink />
    </React.Fragment>
  );
};
export default ForgottenPasswordPage;
