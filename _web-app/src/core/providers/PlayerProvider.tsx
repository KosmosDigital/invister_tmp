import React, {createContext, useState} from "react";
import firebase, {firestore} from "firebase";
import Player from "../models/Player";
import ModelFactory from "../service/model.factory";

export const PlayerContext = createContext<null | Player>(null);

function PlayerProvider(props: any) {
    const [player, setPlayer] = useState<null | Player>(null);

    firestore().collection('players').doc(props.uid).get().then(function(playerRef) {
        setPlayer(ModelFactory.CreatePlayerFromFirestore(playerRef.data()));
    }).catch(function() {
       console.error('player provider error');
    });

    return (
        <PlayerContext.Provider value={player}>{props.children}</PlayerContext.Provider>
    );
}
export default PlayerProvider;