export enum Module {
  TAC = "tactic",
  TEC = "technic",
  ATHL = "athletic",
  MIN = "mental",
  CLASS = "classification",
}
