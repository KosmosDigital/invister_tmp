import {firestore} from "firebase";
import User from "../models/User";
import ModelFactory from "./model.factory";

export default class UserService {
    public static async getUser(uid: string) {
        let userRef = await firestore().collection('users').doc(uid).get();
        return ModelFactory.CreateUserFromFirestore(userRef.data());
    }

    public static async updateUser(user: User) {
        let res = await firestore().collection('users').doc(user.uid).update(ModelFactory.SerializeUserToFirestore(user));
        return res;
    }
}