import { firestore } from "firebase";
import ModelFactory from "./model.factory";
import Player from "../models/Player";

export default class PlayerService {
  public static async getPlayer(uid: string) {
    let playerRef = await firestore().collection("players").doc(uid).get();
    return ModelFactory.CreatePlayerFromFirestore(playerRef.data());
  }

  public static async updatePlayer(player: Player) {
    let res = await firestore()
      .collection("players")
      .doc(player.uid)
      .update(ModelFactory.SerializePlayerToFirestore(player));
    return res;
  }

  public static async listPlayers(treePath: string) {
    let playersRef = await firestore()
      .collection("players")
      .where("owner", ">=", treePath)
      .where("owner", "<=", treePath + "~")
      .get();

    let players: Player[] = [];

    playersRef.forEach((ref) => {
      players.push(ModelFactory.CreatePlayerFromFirestore(ref.data()));
    });

    return players;
  }
}
