import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:invister/core/enums/playing_state.dart';
import 'package:invister/core/models/audio.dart';
import 'package:invister/core/i18n/player/player_audio_note.i18n.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/services/audioplayer_service.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/locator.dart';

class AudioNote extends StatefulWidget {
  AudioNote({this.audio, this.player});

  final Audio? audio;
  final Player? player;

  @override
  _AudioNoteState createState() => _AudioNoteState();
}

class _AudioNoteState extends State<AudioNote> {
  PlayingState playing = PlayingState.Idle;
  bool isDeleted = false;

  final AudioPlayerService audioPlayerService = locator<AudioPlayerService>();

  @override
  void dispose() {
    audioPlayerService.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final DateTime dateTime = DateTime.fromMicrosecondsSinceEpoch(
        widget.audio!.createdAt!.microsecondsSinceEpoch);
    final DateFormat formatter = DateFormat('dd/MM/yyyy');
    final String createdAt = formatter.format(dateTime);

    if (isDeleted) {
      return Container();
    }

    return Row(
      children: [
        Flexible(
            child: MaterialButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey),
          ),
          color: Colors.transparent,
          splashColor: Colors.grey,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                  child: Text(
                'Report of '.i18n +
                    createdAt +
                    ' - By '.i18n +
                    widget.audio!.ownerFullName!,
                textAlign: TextAlign.left,
                style: TextStyle(color: Colors.white),
                overflow: TextOverflow.ellipsis,
              )),
              Icon(
                  playing == PlayingState.Playing
                      ? Icons.pause
                      : Icons.play_arrow,
                  color: Colors.white),
            ],
          ),
          onPressed: () async {
            if (this.playing == PlayingState.Playing) {
              audioPlayerService.pause();
              setState(() {
                playing = PlayingState.Paused;
              });
            } else if (this.playing == PlayingState.Paused) {
              audioPlayerService.resume();
              setState(() {
                playing = PlayingState.Playing;
              });
            } else {
              Stream onCompletion =
                  await audioPlayerService.play(widget.audio!);
              setState(() {
                playing = PlayingState.Playing;
              });

              await onCompletion.first;

              if (!this.mounted) return;
              setState(() {
                playing = PlayingState.Idle;
              });
            }
          },
        )),
        IconButton(
          icon: Icon(Icons.delete),
          color: Colors.white,
          iconSize: 14.0,
          onPressed: () => showDeleteDialog(context, widget.audio!.path!),
        )
      ],
    );
  }

  Future showDeleteDialog(context, String path) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete audio note'.i18n),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Do you want to delete this audio note ?'.i18n),
              ],
            ),
          ),
          actions: <Widget>[
            MaterialButton(
              child: Text('Confirm'.i18n),
              onPressed: () {
                removeAudioAndUpdatePlayer();
                Navigator.pop(context);
              },
            ),
            MaterialButton(
              child: Text('Cancel'.i18n),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  removeAudioAndUpdatePlayer() {
    widget.player!.audioNotes!
        .removeWhere((element) => element.path == widget.audio!.path);
    List<Map<String, dynamic>> audioNotes =
        widget.player!.audioNotes!.map((aN) => aN.toJSON()).toList();

    Document<Player>(path: "players/${widget.player!.uid}").upsert({
      'audioNotes': audioNotes,
    }).then((value) => setState(() => isDeleted = true));
  }
}
