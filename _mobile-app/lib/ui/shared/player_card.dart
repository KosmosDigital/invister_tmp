import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/models/wishlist.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/i18n/player/player_card.i18n.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:provider/provider.dart';

class PlayerCard extends StatefulWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  final Player? player;
  final bool?
      isWishlist; // is the player card displayed in the wishlist or home screen ?

  PlayerCard({this.player, this.isWishlist});

  @override
  _PlayerCardState createState() => _PlayerCardState();
}

class _PlayerCardState extends State<PlayerCard> {
  IconData? starIcon;

  @override
  Widget build(BuildContext context) {
    List<Wishlist>? wishlists = Provider.of<List<Wishlist>?>(context);
    List<Methodology>? methodologies = Provider.of<List<Methodology>?>(context);
    User? me = Provider.of<User?>(context);
    print("==+++================+> ");
    if (wishlists == null || methodologies == null || me == null)
      return Loader();
    print("==+++================+> 111");

    final bool isNew = me.notifications != null &&
        me.notifications!.containsKey('newPlayers') &&
        (me.notifications!['newPlayers'] as List<dynamic>)
            .contains(widget.player!.uid);

    return _showDismissiblePlayerCard(
        wishlists, context, methodologies, isNew, me);
  }

  Widget _showDismissiblePlayerCard(
      List<Wishlist> wishlists,
      BuildContext context,
      List<Methodology> methodologies,
      bool isNew,
      User me) {
    return Slidable(
        key: UniqueKey(),
        child: InkWell(
          onTap: () => handleClickCard(PlayerProfileRoute, isNew, me),
          child: _showStaticPlayerCard(
              context, wishlists, methodologies, isNew, me),
        ),
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        actions: [
          _showTrashBin(context),
        ]);
  }

  Widget _showStaticPlayerCard(BuildContext context, List<Wishlist> wishlists,
      List<Methodology> methodologies, bool isNew, User me) {
    return Container(
      child: Column(
        children: [
          _showPlayerPhotosAndInfos(context, wishlists, methodologies),
          Padding(padding: EdgeInsets.only(top: 10.0)),
          _showNotesButtons(isNew, me),
          SizedBox(height: 10),
          _showSignalsIcons(),
          SizedBox(height: 10)
        ],
        mainAxisAlignment: MainAxisAlignment.start,
      ),
      decoration: ShapeDecoration(
          shadows: [
            isNew
                ? BoxShadow(
                    color: Colors.red,
                    spreadRadius: 1,
                    blurRadius: 15,
                  )
                : BoxShadow(),
            BoxShadow(
              color: gradeColor(widget.player!.grade!),
              offset: Offset.fromDirection(4.71, 5),
            ),
          ],
          color: Colors.black,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.grey))),
    );
  }

  Widget _showPlayerPhotosAndInfos(BuildContext context,
      List<Wishlist> wishlists, List<Methodology> methodologies) {
    bool isWished = wishlists
        .any((wishlist) => wishlist.players!.contains(widget.player!.uid));

    // overly complicated age calculator
    String age =
        (widget.player!.dateOfBirth!.difference(DateTime.now()).inDays /
                    365.2422 *
                    -1)
                .floor()
                .toString() +
            ' years old'.i18n;

    Methodology methodology = methodologies.firstWhere(
        (m) => m.uid == widget.player!.position,
        orElse: () => Methodology(name: ""));

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      textBaseline: TextBaseline.alphabetic,
      children: [
        widget.player!.avatar != null
            ? FutureBuilder(
                future: getAvatar(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData)
                    return CachedNetworkImage(
                      imageUrl: snapshot.data as String,
                      imageBuilder: (context, imageProvider) => Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      fit: BoxFit.cover,
                      height: 75,
                      width: 75,
                    );
                  return Container(height: 75, width: 75);
                })
            : Icon(Icons.account_box, size: 75.0, color: Colors.grey),
        Padding(padding: EdgeInsets.only(left: 12.5)),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(padding: EdgeInsets.only(top: 5.0)),
                      Text(
                          widget.player!.firstName! +
                              ' ' +
                              widget.player!.lastName!,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                      Padding(padding: EdgeInsets.only(top: 2.5)),
                      Text(age,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.white, fontSize: 12.0),
                          textAlign: TextAlign.left),
                      Padding(padding: EdgeInsets.only(top: 1.0)),
                      Text(methodology.name!,
                          overflow: TextOverflow.ellipsis,
                          style:
                              TextStyle(color: Colors.white, fontSize: 12.0)),
                      Padding(padding: EdgeInsets.only(top: 1.0)),
                      Text(widget.player!.club!,
                          overflow: TextOverflow.ellipsis,
                          style:
                              TextStyle(color: Colors.white, fontSize: 12.0)),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: IconButton(
                    icon: Icon(
                      isWished ? Icons.star : Icons.star_border,
                      color: isWished ? Colors.yellowAccent : Colors.white,
                    ),
                    iconSize: 30.0,
                    onPressed: () =>
                        _handleTapFavorite(context, wishlists, isWished)),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _showNotesButtons(bool isNew, User me) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 75.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.description, color: Colors.white, size: 32.0),
                  Text('Report'.i18n,
                      style: TextStyle(fontSize: 12.0, color: Colors.white))
                ])),
        onPressed: () => handleClickCard(PlayerNotesListRoute, isNew, me),
      ),
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 75.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.volume_up, color: Colors.white, size: 32.0),
                  Text('Audio'.i18n,
                      style: TextStyle(fontSize: 12.0, color: Colors.white))
                ])),
        onPressed: () => handleClickCard(PlayerAudiosListRoute, isNew, me),
      ),
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 75.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.person_outline, color: Colors.white, size: 32.0),
                  Text('Evaluation'.i18n,
                      style: TextStyle(fontSize: 12.0, color: Colors.white))
                ])),
        onPressed: () => handleClickCard(PlayerEvaluationsRoute, isNew, me),
      )
    ]);
  }

  Widget _showSignalsIcons() {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
      Icon(
        Icons.arrow_forward,
        color: widget.player!.requestForTransfer! ? PRIMARY_COLOR : Colors.grey,
      ),
      Icon(
        Icons.attach_money,
        color: widget.player!.requestForLoan! ? PRIMARY_COLOR : Colors.grey,
      ),
      Icon(
        Icons.warning,
        color: widget.player!.isUrgent! ? PRIMARY_COLOR : Colors.grey,
      ),
      Icon(
        Icons.remove_red_eye,
        color: widget.player!.toWatch! ? PRIMARY_COLOR : Colors.grey,
      ),
      Icon(
        Icons.edit,
        color: widget.player!.fileReport! ? PRIMARY_COLOR : Colors.grey,
      ),
    ]);
  }

  Widget _showTrashBin(BuildContext context) {
    return SlideAction(
      color: Colors.black,
      child: CircleAvatar(
          backgroundColor: Colors.white24,
          child: Icon(Icons.delete, color: Colors.red)),
      onTap: () => _showDeletePlayerDialog(context),
    );
  }

  void _handleTapFavorite(
      BuildContext context, List<Wishlist> wishlists, bool isWished) {
    if (isWished) {
      Wishlist wishlist = wishlists
          .firstWhere((wl) => wl.players!.contains(widget.player!.uid));
      wishlist.players!.remove(widget.player!.uid);

      Document<Wishlist>(path: 'wishlists/${wishlist.uid}')
          .upsert({'players': wishlist.players});
    } else {
      _showAddWishlistDialog(context, wishlists);
    }
  }

  void _showAddWishlistDialog(BuildContext context, List<Wishlist> wishlists) {
    List<Widget> wishlistsList = [];
    wishlists.forEach((wishlist) {
      wishlistsList.add(ListTile(
        onTap: () {
          if (!wishlist.players!.contains(widget.player!.uid)) {
            wishlist.players!.add(widget.player!.uid!);
            Document<Wishlist>(path: 'wishlists/${wishlist.uid}')
                .upsert({'players': wishlist.players});
          }
          Navigator.of(context).pop();
        },
        title: Text(
          wishlist.name!,
          style: TextStyle(color: Colors.white),
        ),
      ));
    });

    if (wishlistsList.length == 0)
      wishlistsList.add(Center(
        child: Text(
          'You don\'t have any wishlist yet'.i18n,
          style: TextStyle(color: Colors.white),
        ),
      ));

    showDialog(
        context: context,
        builder: (_) => Dialog(
              backgroundColor: Colors.transparent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: Colors.black,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white,
                      blurRadius: 20.0,
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Center(
                        child: Text("Select a wishlist".i18n,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic,
                                fontSize: 24.0,
                                color: Colors.white))),
                    SizedBox(height: 20),
                    ListView(
                      children: wishlistsList,
                      shrinkWrap: true,
                    ),
                  ]),
                ),
              ),
            ));
  }

  void _showDeletePlayerDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) => Dialog(
        backgroundColor: Colors.transparent,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.black,
            boxShadow: [
              BoxShadow(
                color: Colors.white,
                blurRadius: 20.0,
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Center(
                  child: Text(
                widget.isWishlist!
                    ? "You are about to delete this player. If you only want to remove it from a wishlist, just tap on the star."
                        .i18n
                    : "You are about to delete this player. Are you sure ?"
                        .i18n,
                textAlign: TextAlign.justify,
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              )),
              SizedBox(height: 40),
              UglyButton(
                text: 'Confirm'.i18n,
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection('players')
                      .doc(widget.player!.uid)
                      .delete();
                  Navigator.of(context).pop();
                },
              )
            ]),
          ),
        ),
      ),
    );
  }

  Future getAvatar() {
    if (widget.player!.avatar!.isEmpty) return Future.value(null);
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('avatars/${widget.player!.avatar}');
    return ref.getDownloadURL();
  }

  void handleClickCard(String route, bool isNew, User me) {
    if (isNew) {
      me.notifications!['newPlayers']
          .retainWhere((playerUid) => playerUid != widget.player!.uid);
      UserData<User>(collection: 'users')
          .upsert({'notifications': me.notifications});
    }

    widget._navigationService.navigateTo(route, arguments: widget.player);
  }
}
