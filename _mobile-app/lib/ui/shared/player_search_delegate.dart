import 'package:algolia/algolia.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/i18n/player/player_search_delegate.i18n.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/router.dart';
import 'package:provider/provider.dart';

final Algolia algolia = Algolia.init(
  applicationId: 'V5ECK0SXTE',
  apiKey: 'c49983a9c23df7eee3b1f01a9b58caf4',
);

class PlayerSearchDelegate extends SearchDelegate {
  final List<Player>? filterList;
  final List<Methodology>? methodologies;
  AlgoliaQuery? searchQuery;

  PlayerSearchDelegate({this.filterList, this.methodologies});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    User? me = Provider.of<User?>(context);

    searchQuery = algolia.instance
        .index('players')
        .filters('owner: ${me!.uid}')
        .query(query);

    return Column(
      children: <Widget>[
        FutureBuilder(
          future: searchQuery!.getObjects(),
          builder: (BuildContext context,
              AsyncSnapshot<AlgoliaQuerySnapshot> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return new Text('Loading...'.i18n);
              default:
                if (snapshot.hasError) {
                  return Text('Error: '.i18n + snapshot.error.toString());
                } else {
                  return Expanded(
                    child: Container(
                      padding: EdgeInsets.only(top: 8),
                      color: Color.fromRGBO(20, 20, 20, 1),
                      child: ListView.separated(
                        separatorBuilder: (context, index) {
                          final AlgoliaObjectSnapshot result =
                              snapshot.data!.hits[index];

                          if (filterList != null &&
                              !filterList!.any((p) => p.uid == result.objectID))
                            return Container();

                          return Divider(color: Colors.grey);
                        },
                        itemCount: snapshot.data!.hits.length,
                        itemBuilder: (context, index) {
                          final AlgoliaObjectSnapshot result =
                              snapshot.data!.hits[index];

                          if (filterList != null &&
                              !filterList!.any((p) => p.uid == result.objectID))
                            return Container();

                          Player player = Player.fromAlgolia(result);

                          return ListTile(
                            onTap: () => close(context, {
                              'player': player.uid,
                              'route': PlayerProfileRoute
                            }),
                            leading: player.avatar != null
                                ? FutureBuilder(
                                    future: getAvatar(player),
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                              ConnectionState.done &&
                                          snapshot.hasData)
                                        return CachedNetworkImage(
                                          imageUrl: snapshot.data as String,
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  Container(
                                            width: 60.0,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                  image: imageProvider,
                                                  fit: BoxFit.cover),
                                            ),
                                          ),
                                        );
                                      return Container(height: 60, width: 60);
                                    })
                                : Icon(Icons.account_circle,
                                    size: 60.0, color: Colors.grey),
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(player.firstName! + ' ' + player.lastName!,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                                Text(getMethodology(player).name!,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16)),
                                Text(player.club!,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16)),
                              ],
                            ),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.description,
                                    size: 30.0,
                                    color: Colors.grey,
                                  ),
                                  onPressed: () => close(context, {
                                    'player': player.uid,
                                    'route': PlayerNotesListRoute
                                  }),
                                ),
                                SizedBox(width: 5),
                                IconButton(
                                  icon: Icon(
                                    Icons.volume_up,
                                    size: 30.0,
                                    color: Colors.grey,
                                  ),
                                  onPressed: () => close(context, {
                                    'player': player.uid,
                                    'route': PlayerAudiosListRoute
                                  }),
                                ),
                                SizedBox(width: 5),
                                IconButton(
                                  icon: Icon(
                                    Icons.person_outline,
                                    size: 30.0,
                                    color: Colors.grey,
                                  ),
                                  onPressed: () => close(context, {
                                    'player': player.uid,
                                    'route': PlayerEvaluationsRoute
                                  }),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  );
                }
            }
          },
        )
      ],
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }

  Future getAvatar(Player player) {
    if (player.avatar!.isEmpty) return Future.value(null);
    Reference ref =
        FirebaseStorage.instance.ref().child('avatars/${player.avatar}');
    return ref.getDownloadURL();
  }

  Methodology getMethodology(Player player) {
    return this.methodologies!.firstWhere((m) => m.uid == player.position,
        orElse: () => Methodology(name: ""));
  }

  @override
  ThemeData appBarTheme(BuildContext? context) {
    assert(context != null);
    final ThemeData? theme = Theme.of(context!);
    assert(theme != null);
    return theme!.copyWith(
      primaryColor: Colors.black,
      textTheme: theme.textTheme.copyWith(
        headline6: TextStyle(color: Colors.white),
      ),
    );
  }
}
