import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/i18n/player/player_scaffold.i18n.dart';
import 'package:invister/core/models/player_classification.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/ui/screens/player/player_action_screen.dart';
import 'package:invister/ui/screens/player/player_classification_screen.dart';
import 'package:invister/ui/screens/player/player_evaluations_scren.dart';
import 'package:invister/ui/screens/player/player_infos_screen.dart';
import 'package:invister/ui/screens/player/player_profile_screen.dart';
import 'package:provider/provider.dart';

class PlayerScaffold extends StatefulWidget {
  final int? initialIndex;
  final Player? player;

  PlayerScaffold({this.initialIndex, this.player});

  _PlayerScaffoldState createState() => _PlayerScaffoldState();
}

class _PlayerScaffoldState extends State<PlayerScaffold>
    with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController? _tabController;

  final Widget svg = SvgPicture.asset(
    'assets/logo_invister.svg',
    color: Color(0xFFFFFFFF),
  );

  @override
  void initState() {
    _tabController = TabController(
        initialIndex: widget.initialIndex!, length: 5, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    User me = Provider.of<User>(context);
    String hsxId = me.parent == 'none' ? me.uid! : me.parent!.split("_")[0];

    return MultiProvider(
      providers: [
        StreamProvider<Player>.value(
          value: Document<Player>(path: 'players/${widget.player!.uid}')
              .streamData(),
          initialData: Player(),
        ),
        StreamProvider<List<PlayerEvaluation>>.value(
          value: PlayerEvaluationData().list(widget.player!.uid!),
          initialData: [],
        ),
        StreamProvider<List<Methodology>>.value(
          value: MethodologyData().list(hsxId),
          initialData: [],
        ),
        StreamProvider<PlayerClassification>.value(
          value: Document<PlayerClassification>(
                  path: 'players_classifications/${widget.player!.uid}')
              .streamData(),
          initialData: PlayerClassification(),
        ),
      ],
      child: Scaffold(
        key: _scaffoldKey,
        appBar: _showAppBar(),
        body: Padding(
            padding: EdgeInsets.only(top: 40.0),
            child: Stack(
              children: <Widget>[
                _showTabBarView(),
              ],
            )),
      ),
    );
  }

  AppBar _showAppBar() {
    return AppBar(
        bottom: _showTabBar(),
        backgroundColor: Colors.black,
        centerTitle: true,
        title: Row(
          mainAxisSize: MainAxisSize.min, // mandatory to center the title
          children: <Widget>[
            Container(height: 30.0, width: 130.0, child: svg),
          ],
        ));
  }

  PreferredSizeWidget _showTabBar() {
    return TabBar(
      controller: _tabController,
      labelColor: Colors.white,
      unselectedLabelColor: Color(0xFFAFB4C6),
      indicatorColor: Colors.white,
      indicatorSize: TabBarIndicatorSize.tab,
      isScrollable: true,
      tabs: <Widget>[
        Tab(
            key: Key('profil'),
            child: Text('Profile'.i18n,
                style:
                    TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal))),
        Tab(
            key: Key('evaluation'),
            child: Text('Evaluation'.i18n,
                style:
                    TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal))),
        Tab(
            key: Key('classification'),
            child: Text('Classification'.i18n,
                style:
                    TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal))),
        Tab(
            key: Key('infos'),
            child: Text('Infos'.i18n,
                style:
                    TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal))),
        Tab(
            key: Key('actions'),
            child: Text('Actions'.i18n,
                style:
                    TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal)))
      ],
    );
  }

  Widget _showTabBarView() {
    return TabBarView(
      controller: _tabController,
      children: <Widget>[
        PlayerProfileScreen(),
        PlayerEvaluationsScreen(),
        PlayerClassificationScreen(),
        PlayerInfosScreen(),
        PlayerActionScreen()
      ],
    );
  }
}
