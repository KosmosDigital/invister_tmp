import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/ui/screens/wishlist/wishlists_list_screen.dart';
import 'package:invister/ui/screens/home_screen.dart';
import 'package:invister/ui/screens/myaccount_screen.dart';
import 'package:invister/ui/shared/bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/ui/shared/player_search_delegate.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BaseScaffold extends StatefulWidget {
  final int? selectedIndex;
  final bool? showField;

  BaseScaffold({this.selectedIndex, this.showField});

  @override
  _BaseScaffoldState createState() => _BaseScaffoldState();
}

class _BaseScaffoldState extends State<BaseScaffold> {
  final GlobalKey<ScaffoldMessengerState>? scaffoldKey =
      GlobalKey<ScaffoldMessengerState>();

  Widget? bodyWidget;
  List<Widget>? _widgetOptions;
  int? selectedIndex;

  final Widget svg = SvgPicture.asset(
    'assets/logo_invister.svg',
    color: Color(0xFFFFFFFF),
  );

  void _handleItemTapped(indexTapped) {
    setState(() {
      selectedIndex = indexTapped;
      bodyWidget = _widgetOptions![indexTapped];
    });
  }

  @override
  void initState() {
    _widgetOptions = <Widget>[
      MyAccountScreen(scaffoldKey: scaffoldKey),
      HomeScreen(),
      WishlistsListScreen()
    ];

    setState(() {
      selectedIndex = widget.selectedIndex;
      bodyWidget = _widgetOptions![selectedIndex!];
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("=================> 7");
    User? me = Provider.of<User?>(context);

    print("======================> " + me.toString());

    if (me == null) return Loader();

    print("====================> 3333");

    String treePath =
        me.parent == 'none' ? me.uid! : me.parent! + '_' + me.uid!;
    String hsxId = me.parent == 'none' ? me.uid! : me.parent!.split("_")[0];

    return MultiProvider(
      providers: [
        StreamProvider<List<Methodology>>.value(
          value: MethodologyData().list(hsxId),
          initialData: [],
        ),
        StreamProvider<List<Player>>.value(
          value: PlayerData().list(treePath),
          initialData: [],
        ),
      ],
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
              backgroundColor: Colors.black,
              centerTitle: true,
              actions: <Widget>[WorkAroundIcon()],
              title: Row(
                mainAxisSize: MainAxisSize.min, // mandatory to center the title
                children: <Widget>[
                  Container(height: 30.0, width: 130.0, child: svg)
                ],
              )),
          bottomNavigationBar: BottomBar(
              handleItemTapped: _handleItemTapped,
              selectedIndex: selectedIndex!),
          // include the body
          body: bodyWidget),
    );
  }
}

// workaround to get access to the players form the Provider.of
class WorkAroundIcon extends StatelessWidget {
  void _handleSearchTapped(BuildContext context) async {
    List<Player>? players = Provider.of<List<Player>?>(context, listen: false);
    List<Methodology>? methodologies =
        Provider.of<List<Methodology>?>(context, listen: false);
    if (players == null || methodologies == null) return;

    dynamic result = await showSearch(
        context: context,
        delegate: PlayerSearchDelegate(methodologies: methodologies));

    if (result != null && result['player'] is String) {
      Player player =
          players.firstWhere((player) => player.uid == result['player']);
      Navigator.of(context).pushNamed(result['route'], arguments: player);
    }
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      color: Colors.white,
      splashColor: Colors.grey,
      onPressed: () => _handleSearchTapped(context),
      icon: Icon(Icons.search, color: Colors.white),
    );
  }
}
