import 'package:flutter/material.dart';
import 'package:invister/core/models/user.dart';

const Color PRIMARY_COLOR = Color(0xff64B6FF);

Color gradeColor(double grade) {
  if (grade < 0) {
    return Colors.black;
  } else if (grade <= 1) {
    return Color(0xFFE7432A);
  } else if (grade <= 2) {
    return Color(0xFFF2A73D);
  } else if (grade <= 3) {
    return Color(0xFFFEF753);
  } else if (grade <= 4) {
    return Color(0xFF61AE5A);
  } else {
    return Color(0xFF76FC4E);
  }
}

Color ratingStarColor(double value) {
  if (value <= 1) {
    return Color(0xFFE7432A);
  } else if (value <= 2) {
    return Color(0xFFF2A73D);
  } else if (value <= 3) {
    return Color(0xFFFEF753);
  } else if (value <= 4) {
    return Color(0xFF61AE5A);
  } else if (value <= 5) {
    return Color(0xFF76FC4E);
  } else {
    return Colors.blue;
  }
}

Color hierarchyColor(User user) {
  if (user.parent!.split("_").length == 2)
    return Colors.green;
  else if (user.parent == "none")
    return Colors.red;
  else
    return Colors.orange;
}
