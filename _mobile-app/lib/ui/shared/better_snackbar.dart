import 'package:flutter/material.dart';

SnackBar betterSnackBar(String message) {
  return SnackBar(
      content: Text(message),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.white24);
}
