import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:invister/core/enums/playing_state.dart';
import 'package:invister/core/enums/recordingstate.dart';
import 'package:invister/core/models/item.dart';
import 'package:invister/core/models/player_criteria.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/player_item.dart';
import 'package:invister/core/services/audioplayer_service.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/core/services/recording_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:invister/ui/shared/better_snackbar.dart';

class CriteriaWidget extends StatefulWidget {
  final String? playerUid;
  final String? criteria;
  final PlayerCriteria? playerCriteria;
  final PlayerItem? playerItem;
  final Item? item;
  final PlayerEvaluation? playerEvaluation;
  final String? categoryName;

  CriteriaWidget(
      {this.playerUid,
      this.criteria,
      this.playerCriteria,
      this.playerEvaluation,
      this.playerItem,
      this.item,
      this.categoryName});
  PlayerCriteriaState createState() => PlayerCriteriaState();
}

class PlayerCriteriaState extends State<CriteriaWidget> {
  // Record
  RecordingState recordingState = RecordingState.Idle;
  RecordingService recordingService = RecordingService();

  // Audio player
  PlayingState playing = PlayingState.Idle;
  bool recording = false;
  final AudioPlayerService audioPlayerService = locator<AudioPlayerService>();

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();

  Color? starColor;
  Widget? iconWidget;
  bool? showTextWidget = true;
  double? currentGrade;
  String? currentAudioPath;
  String? currentTextNote;

  @override
  void initState() {
    super.initState();
    starColor = Colors.white;
    iconWidget = _showTextInput(widget.playerCriteria!, widget.criteria!);
    currentGrade = 3.0;
  }

  @override
  Widget build(BuildContext context) {
    return _showCriteria();
  }

  Widget _showCriteria() {
    return Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
          Text(widget.criteria!,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          _showRating(widget.playerCriteria!, widget.criteria!),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            IconButton(
                icon: Icon(Icons.description, color: Colors.white),
                onPressed: () => setState(() {
                      showTextWidget = true;
                    })),
            Padding(padding: EdgeInsets.only(right: 5.0)),
            IconButton(
                icon: Icon(Icons.volume_up, color: Colors.white),
                onPressed: () => setState(() {
                      showTextWidget = false;
                    }))
          ]),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          _showInputWidget()
        ]));
  }

  Widget _showInputWidget() {
    if (showTextWidget!) {
      return _showTextInput(widget.playerCriteria!, widget.criteria!);
    } else {
      return _showAudioInput(widget.playerCriteria!, widget.criteria!);
    }
  }

  Widget _showRating(PlayerCriteria? playerCriteria, String? criteria) {
    String? textNote;
    String? audioNote;

    if (playerCriteria != null && playerCriteria.grade != null) {
      setState(() {
        currentGrade = playerCriteria.grade;
        starColor = ratingStarColor(currentGrade!);
      });
    } else {
      setState(() {
        starColor = ratingStarColor(currentGrade!);
      });
    }

    return RatingBar.builder(
      initialRating: currentGrade!,
      minRating: 0,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: starColor,
      ),
      onRatingUpdate: (rating) => ratingUpdate(
          playerCriteria!, rating, textNote!, audioNote!, criteria!),
    );
  }

  Widget _showTextInput(PlayerCriteria? playerCriteria, String? criteria) {
    return TextFormField(
        initialValue: playerCriteria != null ? playerCriteria.textNote : '',
        keyboardType: TextInputType.multiline,
        maxLines: 4,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
            fillColor: Color.fromRGBO(70, 70, 70, 1),
            filled: true,
            border: InputBorder.none),
        cursorColor: Colors.white,
        validator: (value) => value!.isEmpty ? "A note can't be empty" : null,
        onChanged: (text) => {
              setState(() => {currentTextNote = text}),
              textNoteUpdate(widget.playerCriteria!, currentGrade!,
                  currentTextNote!, currentAudioPath!, widget.criteria!)
            });
  }

  Widget _showAudioInput(PlayerCriteria? playerCriteria, String? criteria) {
    if (currentAudioPath != null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Listener(
            onPointerDown: (e) => handlePressAddNote(context),
            onPointerUp: (e) => handleReleaseAddNote(context),
            child: SizedBox(
                height: 80.0,
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: PRIMARY_COLOR)),
                  child: Icon(
                      recordingState == RecordingState.Recording
                          ? Icons.speaker_phone
                          : Icons.mic,
                      color: recordingState == RecordingState.Recording
                          ? Colors.red
                          : PRIMARY_COLOR,
                      size: 40.0),
                  onPressed: null,
                )),
          ),
          SizedBox(width: 10),
          SizedBox(
            height: 80.0,
            child: MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: PRIMARY_COLOR)),
                child: Icon(
                    playing == PlayingState.Playing
                        ? Icons.pause
                        : Icons.play_arrow,
                    color: Colors.white,
                    size: 40.0),
                color: Colors.transparent,
                splashColor: Colors.grey,
                onPressed: () async {
                  // TODO changer le fetch des audionotes qui n'est pas en temps reel
                  if (this.playing == PlayingState.Playing) {
                    audioPlayerService.pause();
                    setState(() {
                      playing = PlayingState.Paused;
                    });
                  } else if (this.playing == PlayingState.Paused) {
                    audioPlayerService.resume();
                    setState(() {
                      playing = PlayingState.Playing;
                    });
                  } else {
                    Stream onCompletion = await audioPlayerService
                        .playFromPath(playerCriteria!.audioNote!);
                    setState(() {
                      playing = PlayingState.Playing;
                    });

                    await onCompletion.first;

                    if (!this.mounted) return;
                    setState(() {
                      playing = PlayingState.Idle;
                    });
                  }
                }),
          ),
        ],
      );
    }

    if (playerCriteria == null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Listener(
            onPointerDown: (e) => handlePressAddNote(context),
            onPointerUp: (e) => handleReleaseAddNote(context),
            child: SizedBox(
                height: 80.0,
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: PRIMARY_COLOR)),
                  child: Icon(
                      recordingState == RecordingState.Recording
                          ? Icons.speaker_phone
                          : Icons.mic,
                      color: recordingState == RecordingState.Recording
                          ? Colors.red
                          : PRIMARY_COLOR,
                      size: 40.0),
                  onPressed: null,
                )),
          ),
          SizedBox(width: 10),
        ],
      );
    }

    if (playerCriteria.audioNote == null || playerCriteria.audioNote == '') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Listener(
            onPointerDown: (e) => handlePressAddNote(context),
            onPointerUp: (e) => handleReleaseAddNote(context),
            child: SizedBox(
                height: 80.0,
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: PRIMARY_COLOR)),
                  child: Icon(
                      recordingState == RecordingState.Recording
                          ? Icons.speaker_phone
                          : Icons.mic,
                      color: recordingState == RecordingState.Recording
                          ? Colors.red
                          : PRIMARY_COLOR,
                      size: 40.0),
                  onPressed: null,
                )),
          ),
          SizedBox(width: 10),
        ],
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Listener(
          onPointerDown: (e) => handlePressAddNote(context),
          onPointerUp: (e) => handleReleaseAddNote(context),
          child: SizedBox(
              height: 80.0,
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: PRIMARY_COLOR)),
                child: Icon(
                    recordingState == RecordingState.Recording
                        ? Icons.speaker_phone
                        : Icons.mic,
                    color: recordingState == RecordingState.Recording
                        ? Colors.red
                        : PRIMARY_COLOR,
                    size: 40.0),
                onPressed: null,
              )),
        ),
        SizedBox(width: 10),
        SizedBox(
          height: 80.0,
          child: MaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: PRIMARY_COLOR)),
              child: Icon(
                  playing == PlayingState.Playing
                      ? Icons.pause
                      : Icons.play_arrow,
                  color: Colors.white,
                  size: 40.0),
              color: Colors.transparent,
              splashColor: Colors.grey,
              onPressed: () async {
                // TODO changer le fetch des audionotes qui n'est pas en temps reel

                if (this.playing == PlayingState.Playing) {
                  audioPlayerService.pause();
                  setState(() {
                    playing = PlayingState.Paused;
                  });
                } else if (this.playing == PlayingState.Paused) {
                  audioPlayerService.resume();
                  setState(() {
                    playing = PlayingState.Playing;
                  });
                } else {
                  Stream onCompletion = await audioPlayerService
                      .playFromPath(playerCriteria.audioNote!);
                  setState(() {
                    playing = PlayingState.Playing;
                  });

                  await onCompletion.first;

                  if (!this.mounted) return;
                  setState(() {
                    playing = PlayingState.Idle;
                  });
                }
              }),
        ),
      ],
    );
  }

  ratingUpdate(PlayerCriteria? playerCriteria, rating, String? textNote,
      String audioNote, String criteria) {
    if (rating is int) {
      rating = rating.toDouble();
    }
    colorChange(rating);
    if (playerCriteria != null) {
      playerCriteria.grade = rating;
    } else {
      if (playerCriteria != null) {
        textNote = playerCriteria.textNote!;
        audioNote = playerCriteria.audioNote!;
      }
      playerCriteria = PlayerCriteria(
          grade: rating,
          textNote: textNote,
          audioNote: audioNote,
          title: criteria);
      if (widget.playerEvaluation != null) {
        widget.playerEvaluation!.categories!.forEach((c) {
          if (c.type == widget.categoryName) {
            // bool true if the right playerCriteria exist
            bool found = false;
            // bool true if the right playerItem exist
            bool found2 = false;
            if (c.items!.isEmpty) {
              List<PlayerCriteria> list = [];
              list.add(playerCriteria!);
              c.items!
                  .add(PlayerItem(title: widget.item!.title, criterias: list));
            } else {
              c.items!.forEach((i) {
                if (i.title == widget.item!.title) {
                  found2 = true;
                  i.criterias!.forEach((crit) {
                    if (crit.title == criteria) {
                      crit.textNote = textNote;
                      crit.audioNote = audioNote;
                      crit.grade = rating;
                      found = true;
                    }
                  });
                  if (!found) {
                    i.criterias!.add(playerCriteria!);
                  }
                }
              });
              if (!found2) {
                List<PlayerCriteria> list = [];
                list.add(playerCriteria!);
                c.items!.add(
                    PlayerItem(title: widget.item!.title, criterias: list));
              }
            }
          }
        });
      } else {
        print('error no playerEvaluation found');
      }
    }
    updatePlayerEvaluation(widget.playerEvaluation!.toJson());
  }

  textNoteUpdate(PlayerCriteria? playerCriteria, double? grade, String? text,
      String audioNote, String criteria) {
    if (playerCriteria != null) {
      playerCriteria.textNote = text;
    } else {
      if (playerCriteria != null) {
        grade = playerCriteria.grade!;
        audioNote = playerCriteria.audioNote!;
      }
      playerCriteria = PlayerCriteria(
          grade: grade, textNote: text, audioNote: audioNote, title: criteria);
      if (widget.playerEvaluation != null) {
        widget.playerEvaluation!.categories!.forEach((c) {
          if (c.type == widget.categoryName) {
            // bool true if the right playerCriteria exist
            bool found = false;
            // bool true if the right playerItem exist
            bool found2 = false;
            if (c.items!.isEmpty) {
              List<PlayerCriteria> list = [];
              list.add(playerCriteria!);
              c.items!
                  .add(PlayerItem(title: widget.item!.title, criterias: list));
            } else {
              c.items!.forEach((i) {
                if (i.title == widget.item!.title) {
                  found2 = true;
                  i.criterias!.forEach((crit) {
                    if (crit.title == criteria) {
                      crit.textNote = text;
                      crit.audioNote = audioNote;
                      crit.grade = grade;
                      found = true;
                    }
                  });
                  if (!found) {
                    i.criterias!.add(playerCriteria!);
                  }
                }
              });
              if (!found2) {
                List<PlayerCriteria> list = [];
                list.add(playerCriteria!);
                c.items!.add(
                    PlayerItem(title: widget.item!.title, criterias: list));
              }
            }
          }
        });
      } else {
        print('error no playerEvaluation found');
      }
    }

    updatePlayerEvaluation(widget.playerEvaluation!.toJson());
  }

  audioNoteUpdate(PlayerCriteria? playerCriteria, double? grade,
      String? textNote, String? audioNote, String? criteria) {
    if (playerCriteria != null) {
      playerCriteria.audioNote = audioNote;
    } else {
      if (playerCriteria != null) {
        grade = playerCriteria.grade!;
        textNote = playerCriteria.textNote!;
      }
      playerCriteria = PlayerCriteria(
          grade: grade,
          textNote: textNote,
          audioNote: audioNote,
          title: criteria);
      if (widget.playerEvaluation != null) {
        widget.playerEvaluation!.categories!.forEach((c) {
          if (c.type == widget.categoryName) {
            // bool true if the right playerCriteria exist
            bool found = false;
            // bool true if the right playerItem exist
            bool found2 = false;
            if (c.items!.isEmpty) {
              List<PlayerCriteria> list = [];
              list.add(playerCriteria!);
              c.items!
                  .add(PlayerItem(title: widget.item!.title, criterias: list));
            } else {
              c.items!.forEach((i) {
                if (i.title == widget.item!.title) {
                  found2 = true;
                  i.criterias!.forEach((crit) {
                    if (crit.title == criteria) {
                      crit.textNote = textNote;
                      crit.audioNote = audioNote;
                      crit.grade = grade;
                      found = true;
                    }
                  });
                  if (!found) {
                    i.criterias!.add(playerCriteria!);
                  }
                }
              });
              if (!found2) {
                List<PlayerCriteria> list = [];
                list.add(playerCriteria!);
                c.items!.add(
                    PlayerItem(title: widget.item!.title, criterias: list));
              }
            }
          }
        });
      } else {
        print('error no playerEvaluation found');
      }
    }
    updatePlayerEvaluation(widget.playerEvaluation!.toJson());
  }

  updatePlayerEvaluation(Map data) async {
    try {
      data['lastCatUpdate'] = widget.categoryName;
      data['lastItemUpdate'] = widget.item!.title;
      data['lastCritUpdate'] = widget.criteria;
      data['flutterUpdate'] = true;

      await PlayerEvaluationData().upsert(data as Map<String, dynamic>,
          widget.playerEvaluation!, widget.playerUid!);
      return 'Evaluation submitted';
    } catch (e) {
      return _errorHandlerService.toFriendlyMessage(e);
    }
  }

  searchAndRemove(String criteria) {
    bool remove = false;

    widget.playerEvaluation!.categories!.forEach((c) {
      if (c.type == widget.categoryName) {
        c.items!.forEach((i) {
          if (i.title == widget.item!.title) {
            i.criterias!.forEach((crit) {
              if (crit.title == criteria) {
                remove = true;
              }
              if (remove) {
                i.criterias!.remove(crit);
              }
              remove = false;
            });
          }
        });
      }
    });
  }

  colorChange(double grade) {
    setState(() {
      currentGrade = grade;
      starColor = ratingStarColor(grade);
    });
  }

  void handlePressAddNote(BuildContext context) async {
    try {
      setState(() => recordingState = RecordingState.Recording);
      await recordingService.start();
    } catch (e) {
      String error = _errorHandlerService.toFriendlyMessage(e);
      ScaffoldMessenger.of(context).showSnackBar(betterSnackBar(error));
      setState(() => recordingState = RecordingState.Idle);
    }
  }

  void handleReleaseAddNote(BuildContext context) async {
    double? grade;
    String? textNote;

    try {
      await recordingService.stop();

      // in case the recording last less than 2 seconds, we ignore it (might be a user mistake)

      Duration? dur = await recordingService.getCurrentDuration();

      if (dur! < Duration(seconds: 2)) {
        setState(() {
          recordingState = RecordingState.Idle;
        });
        return;
      }
    } catch (e) {
      setState(() {
        recordingState = RecordingState.Idle;
      });
      return;
    }

    setState(() => recordingState = RecordingState.Uploading);

    try {
      String path = await recordingService
          .uploadToPlayerEvaluation(widget.playerEvaluation!);
      audioNoteUpdate(
          widget.playerCriteria!, grade!, textNote!, path, widget.criteria!);
      setState(() {
        currentAudioPath = path;
      });
    } catch (e) {
      setState(() {
        recordingState = RecordingState.Idle;
      });
      return;
    }

    setState(() => recordingState = RecordingState.Done);

    Timer(
      Duration(seconds: 2),
      () {
        if (!this.mounted) return;
        setState(() {
          recordingState = RecordingState.Idle;
        });
      },
    );
  }
}
