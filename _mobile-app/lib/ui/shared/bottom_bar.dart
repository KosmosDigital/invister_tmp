import 'package:badges/badges.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:provider/provider.dart';

class BottomBar extends StatelessWidget {
  final handleItemTapped;

  final int? selectedIndex;

  BottomBar({this.handleItemTapped, this.selectedIndex});

  @override
  Widget build(BuildContext context) {
    User? me = Provider.of<User?>(context);

    print("=================> Bottom bar");

    if (me == null) return Loader();

    print("=================> Bottom bar222");

    final int newPlayers =
        me.notifications != null && me.notifications!.containsKey('newPlayers')
            ? me.notifications!['newPlayers'].length
            : 0;

    return BottomNavigationBar(
      backgroundColor: Colors.black,
      iconSize: 34.0,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.account_circle), label: ''),
        BottomNavigationBarItem(
            icon: Badge(
              showBadge: newPlayers > 0,
              badgeContent: Text(newPlayers.toString()),
              child: Icon(Icons.group),
            ),
            label: ''),
        BottomNavigationBarItem(icon: Icon(Icons.star_border), label: '')
      ],
      currentIndex: selectedIndex!,
      selectedItemColor: PRIMARY_COLOR,
      unselectedItemColor: Colors.white,
      onTap: handleItemTapped,
    );
  }
}
