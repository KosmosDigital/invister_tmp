import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/models/wishlist.dart';
import 'package:invister/core/i18n/wishlist/wishlist_list.i18n.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:provider/provider.dart';

class WishlistsListScreen extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    List<Wishlist> wishlists = Provider.of<List<Wishlist>>(context);
    List<Player> players = Provider.of<List<Player>>(context);
    User me = Provider.of<User>(context);

    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 30),
        child: ListView(
          children: [
            _showTitlePage(),
            _showPrimaryButton(context, me),
            _showListOfWishlist(wishlists, players)
          ],
        ),
      ),
    );
  }

  Widget _showTitlePage() {
    return Container(
      height: 25,
      color: Colors.black,
      child: Center(
        child: Text(
          'Your Wishlists'.i18n,
          style: TextStyle(
              color: Colors.white,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 20.0),
        ),
      ),
    );
  }

  Widget _showPrimaryButton(BuildContext context, User me) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 10.0),
        child: SizedBox(
            height: 80.0,
            child: new MaterialButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: Colors.grey)),
              color: Colors.transparent,
              splashColor: Colors.grey,
              child: Container(
                child: Row(
                  children: [
                    Icon(Icons.add_circle, color: PRIMARY_COLOR, size: 35.0),
                    Text(
                      'Add a new wishlist'.i18n,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontStyle: FontStyle.normal),
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                ),
              ),
              onPressed: () => showConfirmDialog(context, me),
            )));
  }

  Widget _showListOfWishlist(List<Wishlist>? wishlists, List<Player> players) {
    List<Widget>? list = [];

    if (wishlists != null) {
      wishlists.forEach((wishlist) {
        list.add(_showWishlistCard(wishlist, players));
      });
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, children: list);
  }

  Widget _showWishlistCard(Wishlist wishlist, List<Player> players) {
    return Container(
      margin: EdgeInsets.only(top: 15.0),
      child: InkWell(
        onTap: () => _navigationService.navigateTo(WishlistRoute,
            arguments: {'wishlistUid': wishlist.uid, 'players': players}),
        child: Slidable(
            key: UniqueKey(),
            child: Container(
                padding: EdgeInsets.only(left: 20.0),
                child: _showWishlistName(wishlist),
                decoration: ShapeDecoration(
                    color: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(color: Colors.grey)))),
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            actions: [
              _showTrashBin(wishlist),
            ]),
      ),
    );
  }

  Widget _showWishlistName(Wishlist wishlist) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(padding: EdgeInsets.only(top: 15.0)),
          Text(wishlist.name!,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              textAlign: TextAlign.left),
          Text(wishlist.description!,
              style: TextStyle(color: Colors.white), textAlign: TextAlign.left),
          Padding(padding: EdgeInsets.only(bottom: 15.0))
        ]);
  }

  Widget _showTrashBin(Wishlist wishlist) {
    return SlideAction(
      color: Colors.black,
      child: CircleAvatar(
          backgroundColor: Colors.white24,
          child: Icon(Icons.delete, color: Colors.red)),
      onTap: () => WishlistData().delete(wishlist),
    );
  }

  Future showConfirmDialog(BuildContext context, User me) {
    final _formKey = GlobalKey<FormState>();
    Map<String, dynamic> wishlistData = Map();

    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.black,
              boxShadow: [
                BoxShadow(
                  color: Colors.white,
                  blurRadius: 20.0,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Create the list you want to add players to.'.i18n,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 16.0),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 10.0),
                  Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          TextFormField(
                            maxLines: 1,
                            keyboardType: TextInputType.text,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                hintText: 'Name'.i18n,
                                hintStyle: TextStyle(color: Colors.grey)),
                            validator: (value) => value!.isEmpty
                                ? "Name can't be empty".i18n
                                : null,
                            onSaved: (value) =>
                                {wishlistData['name'] = value!.trim()},
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            style: TextStyle(color: Colors.white),
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: 'Description'.i18n,
                                hintStyle: TextStyle(color: Colors.grey)),
                            validator: (value) => value!.isEmpty
                                ? "Description can't be empty".i18n
                                : null,
                            onSaved: (value) =>
                                {wishlistData['description'] = value!.trim()},
                          ),
                        ],
                      )),
                  SizedBox(height: 10),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        MaterialButton(
                          color: PRIMARY_COLOR,
                          child: Text('Cancel'.i18n),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        MaterialButton(
                          color: PRIMARY_COLOR,
                          child: Text('Save'.i18n),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _formKey.currentState!.save();
                              wishlistData['players'] = [];
                              wishlistData['user'] = me.uid;
                              WishlistData().post(wishlistData);
                              Navigator.of(context).pop();
                            }
                          },
                        )
                      ])
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
