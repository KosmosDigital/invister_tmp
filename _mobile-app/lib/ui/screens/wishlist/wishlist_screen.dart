import 'package:flutter/material.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/models/wishlist.dart';
import 'package:invister/core/i18n/wishlist/wishlist.i18n.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/ui/shared/player_card.dart';
import 'package:invister/ui/shared/player_search_delegate.dart';
import 'package:provider/provider.dart';

class WishlistScreen extends StatelessWidget {
  WishlistScreen({this.wishlistUid, this.players});

  final String? wishlistUid;
  final List<Player>? players;

  @override
  Widget build(BuildContext context) {
    User me = Provider.of<User>(context);
    List<Wishlist> wishlists = Provider.of<List<Wishlist>>(context);

    Wishlist wishlist = wishlists.firstWhere((w) => w.uid == wishlistUid);

    final retainedPlayers = List<Player>.from(players!);
    retainedPlayers
        .retainWhere((player) => wishlist.players!.contains(player.uid));

    String hsxId = me.parent == 'none' ? me.uid! : me.parent!.split("_")[0];

    return StreamProvider<List<Methodology>>.value(
        value: MethodologyData().list(hsxId),
        initialData: [],
        child: Builder(
          builder: (BuildContext context) => Scaffold(
            appBar: AppBar(
                backgroundColor: Colors.black,
                actions: <Widget>[
                  IconButton(
                    color: Colors.white,
                    splashColor: Colors.grey,
                    onPressed: () =>
                        _handleSearchTapped(context, retainedPlayers),
                    icon: Icon(Icons.search, color: Colors.white),
                  )
                ],
                centerTitle: true,
                title: Text(
                  'INVISTER'.i18n,
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: Colors.white),
                )),
            // include the body
            body: Padding(
              padding: const EdgeInsets.all(15.0),
              child: ListView(
                children: [
                  _showTitlePage(),
                  _showListOfPlayer(retainedPlayers)
                ],
              ),
            ),
          ),
        ));
  }

  Widget _showTitlePage() {
    return Container(
      height: 25,
      color: Colors.black,
      child: Center(
        child: Text(
          'Liked Players'.i18n,
          style: TextStyle(
              color: Colors.white,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 20.0),
        ),
      ),
    );
  }

  Widget _showListOfPlayer(List<Player> players) {
    List<Widget> list = [];
    players.forEach((player) {
      list.add(Padding(
        padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 0.0),
        child: PlayerCard(player: player, isWishlist: true),
      ));
    });

    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, children: list);
  }

  void _handleSearchTapped(BuildContext context, List<Player> players) async {
    List<Methodology>? methodologies =
        Provider.of<List<Methodology>?>(context, listen: false);

    if (methodologies == null) return;

    dynamic result = await showSearch(
        context: context,
        delegate: PlayerSearchDelegate(
            filterList: players, methodologies: methodologies));

    if (result['player'] is String) {
      Player player =
          players.firstWhere((player) => player.uid == result['player']);
      Navigator.of(context).pushNamed(result['route'], arguments: player);
    }
  }
}
