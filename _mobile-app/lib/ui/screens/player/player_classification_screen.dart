import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:invister/core/models/item.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_classification.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/i18n/player/player_classification.i18n.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:provider/provider.dart';

class PlayerClassificationScreen extends StatefulWidget {
  @override
  _PlayerClassificationScreenState createState() =>
      _PlayerClassificationScreenState();
}

class _PlayerClassificationScreenState
    extends State<PlayerClassificationScreen> {
  @override
  Widget build(BuildContext context) {
    Player? player = Provider.of<Player?>(context);
    PlayerClassification? playerClassification =
        Provider.of<PlayerClassification?>(context);

    if (player == null || playerClassification == null) return Loader();

    // if == 0, it means no position has been set yet
    if (player.position!.length == 0)
      return Center(child: Text('Set a position for the player first'.i18n));

    return FutureBuilder(
        future: Document<Methodology>(path: 'methodologies/${player.position}')
            .get(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done ||
              !snapshot.hasData) return Loader();

          Methodology methodology = snapshot.data as Methodology;

          if (methodology.classifications!.length == 0)
            return Center(child: Text('The classification is empty'.i18n));

          return Padding(
              padding: const EdgeInsets.only(left: 30),
              child: ListView.separated(
                  itemBuilder: (context, index) => buildClassification(
                      methodology.classifications![index],
                      playerClassification),
                  separatorBuilder: (context, index) => SizedBox(height: 30),
                  itemCount: methodology.classifications!.length));
        });
  }

  Widget buildClassification(
      Item item, PlayerClassification playerClassification) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(item.title!,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
        SizedBox(height: 10),
        Wrap(
            spacing: 10,
            children: item.criterias!
                .map((criterion) => ChoiceChip(
                      label: Text(criterion,
                          style: TextStyle(
                              fontSize: 15,
                              // is selected
                              color: playerClassification
                                          .classifications![item.title] ==
                                      criterion
                                  ? Colors.black
                                  : Colors.white)),
                      selected:
                          playerClassification.classifications![item.title] ==
                              criterion,
                      selectedColor: Colors.white,
                      shadowColor: Colors.white,
                      elevation: 2,
                      backgroundColor: Colors.black,
                      onSelected: (selected) {
                        if (selected) {
                          playerClassification.classifications![item.title!] =
                              criterion;
                          Document<PlayerClassification>(
                                  path:
                                      'players_classifications/${playerClassification.uid}')
                              .upsert({
                            'classifications':
                                playerClassification.classifications
                          });
                        }
                      },
                    ))
                .toList()),
      ],
    );
  }
}
