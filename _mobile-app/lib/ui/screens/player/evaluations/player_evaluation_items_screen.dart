import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:invister/core/models/item.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/player_item.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/core/i18n/player/player_evaluation.i18n.dart';

class PlayerEvaluationItemsScreen extends StatefulWidget {
  final List<Item>? items;
  final String? type;
  final Player? player;
  final PlayerEvaluation? playerEvaluation;

  PlayerEvaluationItemsScreen(
      {this.items, this.type, this.player, this.playerEvaluation});

  PlayerEvaluationItemsState createState() => PlayerEvaluationItemsState();
}

class PlayerEvaluationItemsState extends State<PlayerEvaluationItemsScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.player == null || widget.playerEvaluation == null)
      return Loader();

    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
            backgroundColor: Colors.black,
            centerTitle: true,
            title: Row(
              mainAxisSize: MainAxisSize.min, // mandatory to center the title
              children: <Widget>[
                Text(
                  '  Invister',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: Colors.white),
                ),
              ],
            )),
        body: Container(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                child: ListView(children: [
                  _showTitlePage(),
                  Padding(padding: EdgeInsets.only(top: 10.0)),
                  _showItems()
                ]))));
  }

  Widget _showTitlePage() {
    IconData? icd;
    String? title;
    if (widget.type == 'tactic') {
      title = 'TACTIC';
      icd = Icons.developer_board;
    } else if (widget.type == 'technic') {
      title = 'TECHNIC';
      icd = Icons.assessment;
    } else if (widget.type == 'mind') {
      title = 'MIND';
      icd = Icons.lightbulb_outline;
    } else if (widget.type == 'athletic') {
      title = 'ATHLETIC';
      icd = Icons.fitness_center;
    }
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Icon(icd, color: Colors.white, size: 50.0),
      Padding(padding: EdgeInsets.only(top: 5.0)),
      Text(title!.i18n,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,
              fontSize: 20.0,
              color: Colors.white))
    ]);
  }

  Widget _showItems() {
    List<Widget> list = [];

    List<PlayerItem> playerItems = [];
    PlayerItem? playerItem;
    if (widget.playerEvaluation != null) {
      widget.playerEvaluation!.categories!.forEach((cat) {
        if (cat.type == widget.type) {
          playerItems = cat.items!;
        }
      });
    }

    widget.items!.forEach((item) {
      list.add(
        MaterialButton(
          elevation: 5.0,
          shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.grey)),
          color: Colors.transparent,
          splashColor: Colors.grey,
          child: Container(
            child: Text(
              item.title!,
              style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.white,
                  fontStyle: FontStyle.normal),
            ),
          ),
          onPressed: () => {
            playerItems.forEach((element) {
              if (element.title == item.title) {
                playerItem = element;
              }
            }),
            _navigationService.navigateTo(PlayerCriteriasRoute, arguments: {
              'playerUid': widget.player!.uid,
              'item': item,
              'playerItem': playerItem,
              'playerEvaluation': widget.playerEvaluation,
              'categoryName': widget.type
            })
          },
        ),
      );
      playerItem = null;
    });

    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, children: list);
  }
}
