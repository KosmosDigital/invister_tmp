import 'package:flutter/material.dart';
import 'package:invister/core/models/item.dart';
import 'package:invister/core/models/player_criteria.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/player_item.dart';
import 'package:invister/ui/shared/criteria_widget.dart';

class PlayerEvaluationCriteriasScreen extends StatefulWidget {
  final String? playerUid;
  final PlayerItem? playerItem;
  final Item? item;
  final PlayerEvaluation? playerEvaluation;
  final String? categoryName;

  PlayerEvaluationCriteriasScreen(
      {this.playerUid,
      this.playerItem,
      this.item,
      this.playerEvaluation,
      this.categoryName});
  PlayerEvaluationCriteriasState createState() =>
      PlayerEvaluationCriteriasState();
}

class PlayerEvaluationCriteriasState
    extends State<PlayerEvaluationCriteriasScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
            backgroundColor: Colors.black,
            centerTitle: true,
            title: Row(
              mainAxisSize: MainAxisSize.min, // mandatory to center the title
              children: <Widget>[
                Text(
                  '  Invister',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: Colors.white),
                ),
              ],
            )),
        body: Container(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                child:
                    ListView(children: [_showTitlePage(), _showCriterias()]))));
  }

  Widget _showTitlePage() {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text('',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 12.0, color: Colors.white))
    ]);
  }

  Widget _showCriterias() {
    List<Widget> list = [];

    widget.item!.criterias!.forEach((element) {
      bool found = false;
      PlayerCriteria? playerCriteria;
      if (widget.playerItem != null) {
        widget.playerItem!.criterias!.forEach((pc) {
          if (pc.title == element) {
            found = true;
            playerCriteria = pc;
          }
        });
      }
      if (found) {
        list.add(CriteriaWidget(
          playerUid: widget.playerUid,
          criteria: element,
          playerCriteria: playerCriteria,
          playerEvaluation: widget.playerEvaluation,
          playerItem: widget.playerItem,
          categoryName: widget.categoryName,
          item: widget.item,
        ));
        found = false;
      } else {
        list.add(CriteriaWidget(
            playerUid: widget.playerUid,
            criteria: element,
            playerCriteria: null,
            playerEvaluation: widget.playerEvaluation,
            playerItem: widget.playerItem,
            categoryName: widget.categoryName,
            item: widget.item));
      }
    });

    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: list);
  }
}
