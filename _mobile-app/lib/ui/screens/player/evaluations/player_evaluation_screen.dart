import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:provider/provider.dart';
import 'package:invister/core/i18n/player/player_evaluation.i18n.dart';

class PlayerEvaluationScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final NavigationService? _navigationService = locator<NavigationService>();
  final PlayerEvaluation? playerEvaluation;
  final Player? player;

  PlayerEvaluationScreen({this.playerEvaluation, this.player});

  @override
  Widget build(BuildContext context) {
    User me = Provider.of<User>(context);
    String hsxId = me.parent == 'none' ? me.uid! : me.parent!.split("_")[0];
    if (playerEvaluation == null)
      return Container(
          child: Center(child: Text('No base evaluation found'.i18n)));

    return MultiProvider(
        providers: [
          StreamProvider<List<Methodology>>.value(
            value: MethodologyData().list(hsxId),
            initialData: [],
          ),
          StreamProvider<PlayerEvaluation>.value(
            value: Document<PlayerEvaluation>(
                    path:
                        'players/${player!.uid}/evaluations/${playerEvaluation!.uid}')
                .streamData(),
            initialData: PlayerEvaluation(),
          )
        ],
        child: Builder(builder: (BuildContext context) {
          List<Methodology>? methodologies =
              Provider.of<List<Methodology>?>(context);
          PlayerEvaluation currentEvaluation =
              Provider.of<PlayerEvaluation>(context);
          if (methodologies == null) {
            return Scaffold(
                key: scaffoldKey,
                appBar: AppBar(
                    backgroundColor: Colors.black,
                    centerTitle: true,
                    title: Row(
                      mainAxisSize:
                          MainAxisSize.min, // mandatory to center the title
                      children: <Widget>[
                        Text(
                          '  Invister',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                              color: Colors.white),
                        ),
                      ],
                    )),
                body: Container(
                    child: Center(child: Text('No methodologies found'.i18n))));
          }
          if (player == null) return Loader();
          if (player!.position == '') {
            return Scaffold(
                key: scaffoldKey,
                appBar: AppBar(
                    backgroundColor: Colors.black,
                    centerTitle: true,
                    title: Row(
                      mainAxisSize:
                          MainAxisSize.min, // mandatory to center the title
                      children: <Widget>[
                        Text(
                          '  Invister',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                              color: Colors.white),
                        ),
                      ],
                    )),
                body: Container(
                    child:
                        Center(child: Text('Select a position first'.i18n))));
          }

          Methodology? methodology;
          methodologies.forEach((element) {
            if (element.uid == player!.position) {
              methodology = element;
            }
          });
          if (methodology == null) {
            return Scaffold(
                key: scaffoldKey,
                appBar: AppBar(
                    backgroundColor: Colors.black,
                    centerTitle: true,
                    title: Row(
                      mainAxisSize:
                          MainAxisSize.min, // mandatory to center the title
                      children: <Widget>[
                        Text(
                          '  Invister',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                              color: Colors.white),
                        ),
                      ],
                    )),
                body: Container(
                    child: Center(
                        child: Text('No methodology found for '.i18n +
                            player!.position!))));
          } else {
            return Scaffold(
                key: scaffoldKey,
                appBar: AppBar(
                    backgroundColor: Colors.black,
                    centerTitle: true,
                    title: Row(
                      mainAxisSize:
                          MainAxisSize.min, // mandatory to center the title
                      children: <Widget>[
                        Text(
                          '  Invister',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                              color: Colors.white),
                        ),
                      ],
                    )),
                body: Container(
                    child: ListView(children: [
                  _showTactAndTech(methodology!, player!, currentEvaluation),
                  Padding(padding: EdgeInsets.only(top: 10.0)),
                  _showPhysAndMental(methodology!, player!, currentEvaluation)
                ])));
          }
        }));
  }

  Widget _showTactAndTech(Methodology methodology, Player player,
      PlayerEvaluation playerEvaluation) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 120.0,
            width: 100.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.developer_board, color: Colors.white, size: 32.0),
                  Text('TACTIC'.i18n,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                          color: Colors.white))
                ])),
        onPressed: () =>
            _navigationService!.navigateTo(PlayerItemsRoute, arguments: {
          'items': methodology.tactic,
          'type': 'tactic',
          'player': player,
          'playerEvaluation': playerEvaluation
        }),
      ),
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 120.0,
            width: 100.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.assessment, color: Colors.white, size: 32.0),
                  Text('TECHNIC'.i18n,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                          color: Colors.white))
                ])),
        onPressed: () =>
            _navigationService!.navigateTo(PlayerItemsRoute, arguments: {
          'items': methodology.technic,
          'type': 'technic',
          'player': player,
          'playerEvaluation': playerEvaluation
        }),
      )
    ]);
  }

  Widget _showPhysAndMental(Methodology methodology, Player player,
      PlayerEvaluation playerEvaluation) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 120.0,
            width: 100.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.fitness_center, color: Colors.white, size: 32.0),
                  Text('ATHLETIC'.i18n,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                          color: Colors.white))
                ])),
        onPressed: () =>
            _navigationService!.navigateTo(PlayerItemsRoute, arguments: {
          'items': methodology.athletic,
          'type': 'athletic',
          'player': player,
          'playerEvaluation': playerEvaluation
        }),
      ),
      MaterialButton(
        elevation: 5.0,
        color: Colors.transparent,
        splashColor: Colors.grey,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        child: Container(
            height: 120.0,
            width: 100.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.lightbulb_outline,
                      color: Colors.white, size: 32.0),
                  Text('MIND'.i18n,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                          color: Colors.white))
                ])),
        onPressed: () =>
            _navigationService!.navigateTo(PlayerItemsRoute, arguments: {
          'items': methodology.mind,
          'type': 'mind',
          'player': player,
          'playerEvaluation': playerEvaluation
        }),
      )
    ]);
  }
}
