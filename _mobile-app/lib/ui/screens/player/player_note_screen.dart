import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:invister/core/models/note.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/i18n/player/player_note.i18n.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/ui/shared/better_snackbar.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:provider/provider.dart';

class PlayerNoteScreen extends StatelessWidget {
  final Note? note;
  final String? uidPlayer;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldMessengerState> scaffoldKey =
      GlobalKey<ScaffoldMessengerState>();
  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();
  final NavigationService _navigationService = locator<NavigationService>();

  final Widget svg = SvgPicture.asset(
    'assets/logo_invister.svg',
    color: Color(0xFFFFFFFF),
  );

  final Map<String, dynamic> noteData = Map();

  PlayerNoteScreen({this.note, this.uidPlayer});

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);

    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
            backgroundColor: Colors.black,
            centerTitle: true,
            title: Row(
              mainAxisSize: MainAxisSize.min, // mandatory to center the title
              children: <Widget>[
                Container(height: 30.0, width: 130.0, child: svg),
              ],
            )),
        body: Container(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                child: Form(
                  key: _formKey,
                  child: ListView(children: [
                    _showTextInput(),
                    Padding(padding: EdgeInsets.only(top: 30.0)),
                    _showPrimaryButton(user, scaffoldKey)
                  ]),
                ))));
  }

  Widget _showTextInput() {
    return TextFormField(
      initialValue: note != null ? note!.text : '',
      keyboardType: TextInputType.multiline,
      maxLines: 20,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          fillColor: Color.fromRGBO(70, 70, 70, 1),
          filled: true,
          border: InputBorder.none),
      cursorColor: Colors.white,
      validator: (value) =>
          value!.isEmpty ? "A note can't be empty".i18n : null,
      onSaved: (value) => noteData['text'] = value,
    );
  }

  Widget _showPrimaryButton(
      User user, GlobalKey<ScaffoldMessengerState> scaffoldKey) {
    return Padding(
        padding: EdgeInsets.fromLTRB(15.0, 45.0, 15.0, 0.0),
        child: UglyButton(
          text: 'Save'.i18n,
          onPressed: () => submitForm(noteData, user, scaffoldKey),
        ));
  }

// -------------------------- METHODS --------------------------

  void submitForm(Map<String, dynamic> data, User user,
      GlobalKey<ScaffoldMessengerState>? scaffoldKey) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      String result = '';

      if (note != null) {
        result = await putNote(data);
      } else {
        result = await postNote(data, user);
      }

      if (scaffoldKey != null) {
        scaffoldKey.currentState!.showSnackBar(betterSnackBar(result));
      }
    }
  }

  Future<String> postNote(Map<String, dynamic> data, User user) async {
    try {
      data['owner'] = user.uid;
      data['ownerName'] = user.firstName! + ' ' + user.lastName!;
      data['createdAt'] = Timestamp.now();

      await NoteData(uidPlayer: uidPlayer!).post(data);

      _navigationService.goBack();

      return 'Note created'.i18n;
    } catch (e) {
      return _errorHandlerService.toFriendlyMessage(e);
    }
  }

  Future<String> putNote(Map data) async {
    try {
      await NoteData(uidPlayer: uidPlayer!).upsert(data, note!);
      return 'Note updated'.i18n;
    } catch (e) {
      return _errorHandlerService.toFriendlyMessage(e);
    }
  }
}
