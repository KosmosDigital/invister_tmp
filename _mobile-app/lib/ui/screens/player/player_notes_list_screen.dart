import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:invister/core/models/note.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:provider/provider.dart';
import 'package:invister/core/i18n/player/player_note_list.i18n.dart';

class PlayerStreamScreen extends StatelessWidget {
  final Player? player;

  PlayerStreamScreen({this.player});

  @override
  Widget build(BuildContext context) {
    User? me = Provider.of<User?>(context);

    String? playerOwner = player!.owner!.split("_").last;

    if (me == null) return Loader();

    String? treePath =
        me.parent == 'none' ? me.uid! : me.parent! + '_' + me.uid!;

    return MultiProvider(providers: [
      StreamProvider<List<Note>>.value(
        value: NoteData(uidPlayer: player!.uid!).list(treePath),
        initialData: [],
      ),
      StreamProvider<User>.value(
        value: Document<User>(path: 'users/$playerOwner').streamData(),
        initialData: User(),
      ),
    ], child: PlayerNotesListScreen(uidPlayer: player!.uid));
  }
}

class PlayerNotesListScreen extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  final String? uidPlayer;

  PlayerNotesListScreen({this.uidPlayer});

  final Widget svg = SvgPicture.asset(
    'assets/logo_invister.svg',
    color: Color(0xFFFFFFFF),
  );

  @override
  Widget build(BuildContext context) {
    List<Note> notes = Provider.of<List<Note>>(context);
    User? playerOwner = Provider.of<User?>(context);

    if (playerOwner == null) return Loader();

    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.black,
            centerTitle: true,
            title: Row(
              mainAxisSize: MainAxisSize.min, // mandatory to center the title
              children: <Widget>[
                Container(height: 30.0, width: 130.0, child: svg),
              ],
            )),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _showPrimaryButton(),
                _showScoutName(playerOwner),
                _showListOfNotes(notes, context)
              ],
            ),
          ),
        ));
  }

  Widget _showPrimaryButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 10.0),
        child: SizedBox(
            height: 80.0,
            child: new MaterialButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: Colors.grey)),
              color: Colors.transparent,
              splashColor: Colors.grey,
              child: Container(
                child: Row(
                  children: [
                    Icon(Icons.add_circle, color: PRIMARY_COLOR, size: 35.0),
                    Text(
                      'Add a new note'.i18n,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontStyle: FontStyle.normal),
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                ),
              ),
              onPressed: () => _navigationService.navigateTo(PlayerNoteRoute,
                  arguments: {'uidPlayer': uidPlayer}),
            )));
  }

  Widget _showScoutName(User playerOwner) {
    return Container(
        padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Text(
              'Follow by: '.i18n +
                  playerOwner.firstName! +
                  ' ' +
                  playerOwner.lastName!,
              style: TextStyle(color: Colors.white)),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          Icon(
            Icons.supervisor_account,
            color: hierarchyColor(playerOwner),
          )
        ]));
  }

  Widget _showListOfNotes(List<Note>? notes, context) {
    List<Widget> list = [];
    print(notes);
    if (notes != null) {
      notes.forEach((note) {
        list.add(_showNotesSummary(note, context));
      });
    }

    return Expanded(child: ListView(children: list));
  }

  Widget _showNotesSummary(Note note, context) {
    DateTime? date = DateTime.tryParse(note.createdAt!.toDate().toString());

    return Row(children: [
      Flexible(
          child: MaterialButton(
        elevation: 5.0,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        color: Colors.transparent,
        splashColor: Colors.grey,
        child: Text(
          'Rapport of '.i18n +
              DateFormat('dd/MM/yyyy').format(date!) +
              ' - By '.i18n +
              note.ownerName!,
          textAlign: TextAlign.left,
          style: TextStyle(color: Colors.white),
          overflow: TextOverflow.ellipsis,
        ),
        onPressed: () => _navigationService.navigateTo(PlayerNoteRoute,
            arguments: {'uidPlayer': uidPlayer, 'note': note}),
      )),
      IconButton(
        icon: Icon(Icons.delete),
        color: Colors.white,
        iconSize: 14.0,
        onPressed: () => showDeleteDialog(note, context),
      )
    ]);
  }

  Future showDeleteDialog(Note note, context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete note'.i18n),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Do you want to delete this note ?'.i18n),
              ],
            ),
          ),
          actions: <Widget>[
            MaterialButton(
              child: Text('Confirm'.i18n),
              onPressed: () {
                NoteData(uidPlayer: uidPlayer!).delete(note.uid!);
                Navigator.pop(context);
              },
            ),
            MaterialButton(
              child: Text('Cancel'.i18n),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }
}
