import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:invister/core/enums/recordingstate.dart';
import 'package:invister/core/i18n/player/player_audio_list.i18n.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/core/services/recording_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:invister/ui/shared/better_snackbar.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/ui/shared/player_audio_note.dart';
import 'package:provider/provider.dart';

class PlayerAudiosListScreen extends StatefulWidget {
  final Player? player;

  PlayerAudiosListScreen({this.player});

  @override
  _PlayerAudiosListScreenState createState() => _PlayerAudiosListScreenState();
}

class _PlayerAudiosListScreenState extends State<PlayerAudiosListScreen> {
  RecordingState recordingState = RecordingState.Idle;
  RecordingService recordingService = RecordingService();
  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();

  final Widget svg = SvgPicture.asset(
    'assets/logo_invister.svg',
    color: Color(0xFFFFFFFF),
  );

  @override
  Widget build(BuildContext context) {
    User? me = Provider.of<User?>(context);

    if (me == null) return Loader();

    String playerOwner = widget.player!.owner!.split("_").last;

    String btnCaption;
    if (recordingState == RecordingState.Idle)
      btnCaption = 'Add a new audio note'.i18n;
    else if (recordingState == RecordingState.Recording)
      btnCaption = 'Recording...'.i18n;
    else if (recordingState == RecordingState.Uploading)
      btnCaption = 'Uploading...'.i18n;
    else
      btnCaption = 'Done!'.i18n;

    return StreamProvider<User>.value(
      value: Document<User>(path: 'users/$playerOwner').streamData(),
      initialData: User(),
      child: Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.black,
              centerTitle: true,
              title: Row(
                mainAxisSize: MainAxisSize.min, // mandatory to center the title
                children: <Widget>[
                  Container(height: 30.0, width: 130.0, child: svg),
                ],
              )),
          body: Builder(
            builder: (BuildContext context) {
              return Container(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      _showPrimaryButton(context, btnCaption, me),
                      _showScoutName(context),
                      _showListOfAudios()
                    ],
                  ),
                ),
              );
            },
          )),
    );
  }

  Widget _showPrimaryButton(BuildContext context, String btnCaption, User me) {
    return Padding(
        padding: EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 10.0),
        child: Listener(
          onPointerDown: (e) => handlePressAddNote(context),
          onPointerUp: (e) => handleReleaseAddNote(context, me),
          child: SizedBox(
              height: 80.0,
              child: MaterialButton(
                elevation: 5.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(color: Colors.grey)),
                color: Colors.transparent,
                splashColor: Colors.grey,
                child: Row(
                  children: [
                    SizedBox(width: 10),
                    Icon(Icons.mic, color: PRIMARY_COLOR, size: 35.0),
                    SizedBox(width: 15),
                    Text(
                      btnCaption,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontStyle: FontStyle.normal),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.start,
                ),
                onPressed: null,
              )),
        ));
  }

  Widget _showScoutName(BuildContext context) {
    User? playerOwner = Provider.of<User?>(context);

    if (playerOwner == null) return Loader();

    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Text(
            'Follow by: '.i18n +
                playerOwner.firstName! +
                ' ' +
                playerOwner.lastName!,
            style: TextStyle(color: Colors.white)),
        Padding(padding: EdgeInsets.only(top: 5.0)),
        Icon(Icons.supervisor_account, color: hierarchyColor(playerOwner)),
      ]),
    );
  }

  Widget _showListOfAudios() {
    List<Widget> list = [];

    widget.player!.audioNotes!.forEach((audio) {
      list.add(AudioNote(audio: audio, player: widget.player));
    });

    return Expanded(child: ListView(children: list));
  }

  void handlePressAddNote(BuildContext context) async {
    try {
      setState(() => recordingState = RecordingState.Recording);
      await recordingService.start();
    } catch (e) {
      String error = _errorHandlerService.toFriendlyMessage(e);
      ScaffoldMessenger.of(context).showSnackBar(betterSnackBar(error));
      setState(() => recordingState = RecordingState.Idle);
    }
  }

  void handleReleaseAddNote(BuildContext context, User me) async {
    try {
      await recordingService.stop();

      // in case the recording last less than 2 seconds, we ignore it (might be a user mistake)
      Duration? dur = await recordingService.getCurrentDuration();

      if (dur! < Duration(seconds: 2)) {
        setState(() {
          recordingState = RecordingState.Idle;
        });
        return;
      }
    } catch (e) {
      setState(() {
        recordingState = RecordingState.Idle;
      });
      return;
    }

    setState(() => recordingState = RecordingState.Uploading);

    try {
      await recordingService.uploadToPlayer(widget.player!, me);
    } catch (e) {
      setState(() {
        recordingState = RecordingState.Idle;
      });
      return;
    }

    setState(() => recordingState = RecordingState.Done);

    Timer(
      Duration(seconds: 2),
      () {
        if (!this.mounted) return;
        setState(() {
          recordingState = RecordingState.Idle;
        });
      },
    );
  }
}
