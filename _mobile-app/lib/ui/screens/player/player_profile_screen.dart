import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:invister/core/enums/sex.dart';
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:invister/core/i18n/player/player_profile.i18n.dart';
import 'package:provider/provider.dart';

class PlayerProfileScreen extends StatefulWidget {
  PlayerProfileScreen();

  @override
  _PlayerProfileScreenState createState() => _PlayerProfileScreenState();
}

class _PlayerProfileScreenState extends State<PlayerProfileScreen> {
  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();

  final _formKey = GlobalKey<FormState>();

  final picker = ImagePicker();

  TextEditingController? dateOfBirthCtrl;
  bool busyBee = false;

  @override
  Widget build(BuildContext context) {
    Player? player = Provider.of<Player?>(context);
    List<Methodology>? methodologies = Provider.of<List<Methodology>?>(context);

    if (player == null || methodologies == null) return Loader();

    if (dateOfBirthCtrl == null) {
      setState(() {
        dateOfBirthCtrl = TextEditingController.fromValue(TextEditingValue(
            text: "${player.dateOfBirth!.toLocal()}".split(' ')[0]));
      });
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          showForm(player, methodologies),
          SizedBox(height: 20),
          UglyButton(
            text: 'Save'.i18n,
            onPressed: busyBee
                ? null
                : () {
                    _handleSaveForm(player, context);
                  },
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Form showForm(Player player, List<Methodology> methodologies) {
    return Form(
      key: _formKey,
      child: Expanded(
        child: ListView(
          shrinkWrap: true,
          children: [
            showUploadZone(player),
            showLastnameInput(player),
            showFirstnameInput(player),
            showSexInput(player),
            showPositionInput(player, methodologies),
            showDateOfBirthInput(player),
            showPlaceOfBirthInput(player),
            showClubInput(player),
            showChampionshipInput(player),
            showSizeInput(player),
            showWeightInput(player),
            showStrongfootInput(player),
            showCitizenshipInput(player),
            showInjuriesInput(player),
          ],
        ),
      ),
    );
  }

  Widget showUploadZone(Player player) {
    return GestureDetector(
      onTap: () => uploadImage(player),
      child: DottedBorder(
        color: Colors.grey,
        borderType: BorderType.RRect,
        radius: Radius.circular(8.0),
        dashPattern: [6, 6],
        child: Row(
          children: <Widget>[
            player.avatar != null
                ? FutureBuilder(
                    future: getAvatar(player),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.hasData)
                        return CachedNetworkImage(
                          imageUrl: snapshot.data as String,
                          fit: BoxFit.cover,
                          imageBuilder: (context, imageProvider) => Container(
                            width: 90.0,
                            height: 90.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                          height: 90,
                          width: 90,
                        );
                      return Container(height: 90, width: 90);
                    })
                : Icon(Icons.account_box, size: 90.0, color: Colors.grey),
            Expanded(
              child: Center(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Upload a new image'.i18n),
                  SizedBox(width: 10),
                  Icon(
                    Icons.cloud_upload,
                    color: Colors.white,
                  )
                ],
              )),
            )
          ],
        ),
      ),
    );
  }

  Widget showLastnameInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 20),
        Text('Lastname'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.lastName,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (lastname) => player.lastName = lastname!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showFirstnameInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Firstname'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.firstName,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (firstname) => player.firstName = firstname!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showSexInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Sex'.i18n),
        SizedBox(height: 10),
        Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Color.fromRGBO(70, 70, 70, 1),
          ),
          child: DropdownButtonFormField(
            value: player.sex,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                labelStyle: TextStyle(color: Colors.pink),
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            items: [
              DropdownMenuItem(child: Text(Sex.MALE.string), value: Sex.MALE),
              DropdownMenuItem(
                  child: Text(Sex.FEMALE.string), value: Sex.FEMALE)
            ],
            onChanged: (v) => null,
            onSaved: (sex) => player.sex = sex as Sex?,
          ),
        ),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showPositionInput(Player player, List<Methodology> methodologies) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Position'.i18n),
        SizedBox(height: 10),
        Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Color.fromRGBO(70, 70, 70, 1),
          ),
          child: player.position == "" ||
                  !methodologies
                      .any((methodology) => methodology.uid == player.position)
              ? DropdownButtonFormField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      labelStyle: TextStyle(color: Colors.pink),
                      fillColor: Color.fromRGBO(70, 70, 70, 1),
                      filled: true,
                      border: InputBorder.none),
                  items: methodologies
                      .map((methodology) => DropdownMenuItem(
                          child: Text(methodology.name!),
                          value: methodology.uid))
                      .toList(),
                  onChanged: (v) => null,
                  onSaved: (position) => player.position = position as String,
                )
              : DropdownButtonFormField(
                  value: player.position,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      labelStyle: TextStyle(color: Colors.pink),
                      fillColor: Color.fromRGBO(70, 70, 70, 1),
                      filled: true,
                      border: InputBorder.none),
                  items: methodologies
                      .map((methodology) => DropdownMenuItem(
                          child: Text(methodology.name!),
                          value: methodology.uid))
                      .toList(),
                  onChanged: (v) => null,
                  onSaved: (position) => player.position = position as String,
                ),
        ),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showDateOfBirthInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Date of birth'.i18n),
        SizedBox(height: 10),
        TextField(
            controller: dateOfBirthCtrl,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              _selectDate(player);
            },
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white),
        SizedBox(height: 30),
      ],
    );
  }

  Future _selectDate(Player player) async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(1950),
        lastDate: new DateTime(2030));
    if (picked != null) {
      player.dateOfBirth = picked;
      setState(() => dateOfBirthCtrl!.value =
          TextEditingValue(text: "${picked.toLocal()}".split(' ')[0]));
    }
  }

  Widget showPlaceOfBirthInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Place of birth'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.placeOfBirth,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (pob) => player.placeOfBirth = pob!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showClubInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Club'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.club,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (club) => player.club = club!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showChampionshipInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Championship'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.championship,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (championship) =>
                player.championship = championship!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showSizeInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Size'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.size,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (size) => player.size = size!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showWeightInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Weight'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.weight,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (weight) => player.weight = weight!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showStrongfootInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Strong foot'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.strongFoot,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (strongFoot) => player.strongFoot = strongFoot!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showCitizenshipInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Citizenship'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.citizenship,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (citizenship) => player.citizenship = citizenship!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showInjuriesInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Injuries'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.injuries,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (injuries) => player.injuries = injuries!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  // METHODS

  _handleSaveForm(Player player, BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      await updatePlayer(player);
      Navigator.of(context).pop();
    }
  }

  Future<String> updatePlayer(Player player) async {
    try {
      setState(() {
        busyBee = true;
      });

      await Document<Player>(path: 'players/${player.uid}').upsert({
        'firstName': player.firstName,
        'lastName': player.lastName,
        'sex': player.sex!.serialize,
        'position': player.position,
        'dateOfBirth': Timestamp.fromDate(player.dateOfBirth!),
        'placeOfBirth': player.placeOfBirth,
        'club': player.club,
        'championship': player.championship,
        'size': player.size,
        'weight': player.weight,
        'strongFoot': player.strongFoot,
        'citizenship': player.citizenship,
        'injuries': player.injuries,
        'flutterUpdate': true
      });

      setState(() {
        busyBee = false;
      });
      return "Player successfully updated".i18n;
    } catch (e) {
      setState(() {
        busyBee = false;
      });

      return _errorHandlerService.toFriendlyMessage(e);
    }
  }

  uploadImage(Player player) async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile == null) return;

    final image = File(pickedFile.path);
    final fileName = image.path.split('/').last;

    Reference ref = FirebaseStorage.instance.ref().child('avatars/' + fileName);
    UploadTask uploadTask = ref.putFile(image);
    await uploadTask;

    Document<Player>(path: 'players/${player.uid}')
        .upsert({'avatar': fileName});
  }

  Future getAvatar(Player player) {
    if (player.avatar!.isEmpty) return Future.value(null);
    Reference ref =
        FirebaseStorage.instance.ref().child('avatars/${player.avatar}');
    return ref.getDownloadURL();
  }
}
