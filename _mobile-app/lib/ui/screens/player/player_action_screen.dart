import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:invister/core/i18n/player/player_action.i18n.dart';
import 'package:provider/provider.dart';

class PlayerActionScreen extends StatefulWidget {
  PlayerActionScreen();

  @override
  _PlayerActionScreenState createState() => _PlayerActionScreenState();
}

class _PlayerActionScreenState extends State<PlayerActionScreen> {
  final _formKey = GlobalKey<FormState>();

  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();

  bool busyBee = false;

  @override
  Widget build(BuildContext context) {
    Player player = Provider.of<Player>(context);
    User me = Provider.of<User>(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        showForm(player, me),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0, left: 20, right: 20),
          child: showPrimaryButton(player, context),
        )
      ],
    );
  }

  Widget showForm(Player player, User me) {
    return Form(
      key: _formKey,
      child: Expanded(
          child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 30),
              children: [
            me.parent == "none"
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Request a transfer'.i18n,
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold)),
                      Switch(
                        value: player.requestForTransfer!,
                        onChanged: (bool value) {
                          player.requestForTransfer = value;
                          setState(() {});
                        },
                        activeColor: Colors.blue,
                        activeTrackColor: Colors.lightBlueAccent,
                        inactiveTrackColor: Colors.white,
                      )
                    ],
                  )
                : Container(),
            me.parent == "none"
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Request a loan'.i18n,
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold)),
                      Switch(
                        value: player.requestForLoan!,
                        onChanged: (bool value) {
                          player.requestForLoan = value;
                          setState(() {});
                        },
                        activeColor: Colors.blue,
                        activeTrackColor: Colors.lightBlueAccent,
                        inactiveTrackColor: Colors.white,
                      )
                    ],
                  )
                : Container(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Urgent'.i18n,
                    style:
                        TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                Switch(
                  value: player.isUrgent!,
                  onChanged: (bool value) {
                    player.isUrgent = value;
                    setState(() {});
                  },
                  activeColor: Colors.blue,
                  activeTrackColor: Colors.lightBlueAccent,
                  inactiveTrackColor: Colors.white,
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('File a report'.i18n,
                    style:
                        TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                Switch(
                  value: player.fileReport!,
                  onChanged: (bool value) {
                    player.fileReport = value;
                    setState(() {});
                  },
                  activeColor: Colors.blue,
                  activeTrackColor: Colors.lightBlueAccent,
                  inactiveTrackColor: Colors.white,
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('To watch'.i18n,
                    style:
                        TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                Switch(
                  value: player.toWatch!,
                  onChanged: (bool value) {
                    player.toWatch = value;
                    setState(() {});
                  },
                  activeColor: Colors.blue,
                  activeTrackColor: Colors.lightBlueAccent,
                  inactiveTrackColor: Colors.white,
                )
              ],
            ),
          ])),
    );
  }

  Widget showPrimaryButton(Player player, BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 35.0),
        child: UglyButton(
          onPressed: busyBee
              ? null
              : () {
                  _handleSaveForm(player, context);
                },
          text: 'Save'.i18n,
        ));
  }

// METHODS

  _handleSaveForm(Player player, BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      await updatePlayer(player);
      Navigator.of(context).pop();
    }
  }

  Future<String> updatePlayer(Player player) async {
    try {
      setState(() {
        busyBee = true;
      });

      await Document<Player>(path: 'players/${player.uid}').upsert({
        'requestForLoan': player.requestForLoan,
        'requestForTransfer': player.requestForTransfer,
        'isUrgent': player.isUrgent,
        'fileReport': player.fileReport,
        'toWatch': player.toWatch,
        'flutterUpdate': true
      });

      setState(() {
        busyBee = false;
      });
      return "Player successfully updated".i18n;
    } catch (e) {
      setState(() {
        busyBee = false;
      });

      return _errorHandlerService.toFriendlyMessage(e);
    }
  }
}
