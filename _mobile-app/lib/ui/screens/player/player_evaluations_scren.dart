import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/core/i18n/player/player_evaluations.i18n.dart';

class PlayerEvaluationsScreen extends StatefulWidget {
  PlayerEvaluationsScreen();

  @override
  _PlayerEvaluationsScreenState createState() =>
      _PlayerEvaluationsScreenState();
}

class _PlayerEvaluationsScreenState extends State<PlayerEvaluationsScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<PlayerEvaluation> playerEvaluations =
        Provider.of<List<PlayerEvaluation>>(context);
    User me = Provider.of<User>(context);

    Player? player = Provider.of<Player?>(context);
    if (player == null) return Loader();

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _showPrimaryButton(player, me),
          _showListOfPlayerEvaluation(playerEvaluations, player, context),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _showPrimaryButton(Player player, User user) {
    return Padding(
        padding: EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 10.0),
        child: SizedBox(
            height: 80.0,
            child: new MaterialButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: Colors.grey)),
              color: Colors.transparent,
              splashColor: Colors.grey,
              child: Container(
                child: Row(
                  children: [
                    Icon(Icons.add_circle, color: PRIMARY_COLOR, size: 35.0),
                    Text(
                      'New Evaluation'.i18n,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontStyle: FontStyle.normal),
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                ),
              ),
              onPressed: () => PlayerEvaluationData().post(player, user),
            )));
  }

  Widget _showListOfPlayerEvaluation(
      List<PlayerEvaluation>? playerEvaluations, Player player, context) {
    List<Widget> list = [];
    if (playerEvaluations != null) {
      playerEvaluations.forEach((playerEvaluation) {
        list.add(
            _showPlayerEvaluationSummary(player, playerEvaluation, context));
      });
    }

    return Expanded(child: ListView(children: list));
  }

  Widget _showPlayerEvaluationSummary(
      Player player, PlayerEvaluation playerEvaluation, context) {
    DateTime? date =
        DateTime.tryParse(playerEvaluation.createdAt!.toDate().toString());

    return Row(children: [
      Flexible(
          child: MaterialButton(
        elevation: 5.0,
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey)),
        color: Colors.transparent,
        splashColor: Colors.grey,
        child: Text(
          'Evaluation of '.i18n +
              DateFormat('dd/MM/yyyy').format(date!) +
              ' - By '.i18n +
              playerEvaluation.ownerName!,
          textAlign: TextAlign.left,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        ),
        onPressed: () => _navigationService
            .replaceAndNavigateTo(PlayerEvaluationRoute, arguments: {
          'playerEvaluation': playerEvaluation,
          'player': player
        }),
      )),
      IconButton(
        icon: Icon(Icons.delete),
        color: Colors.white,
        iconSize: 14.0,
        onPressed: () => showDeleteDialog(player, playerEvaluation, context),
      )
    ]);
  }

  Future showDeleteDialog(
      Player player, PlayerEvaluation playerEvaluation, context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete evaluation'.i18n),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Do you want to delete this evaluation ?'.i18n),
              ],
            ),
          ),
          actions: <Widget>[
            MaterialButton(
              child: Text('Confirm'.i18n),
              onPressed: () {
                PlayerEvaluationData().delete(player, playerEvaluation.uid!);
                Navigator.pop(context);
              },
            ),
            MaterialButton(
              child: Text('Cancel'.i18n),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }
}
