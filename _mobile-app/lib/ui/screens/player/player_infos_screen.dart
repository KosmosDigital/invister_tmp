import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:invister/core/i18n/player/player_infos.18n.dart';
import 'package:provider/provider.dart';

class PlayerInfosScreen extends StatefulWidget {
  PlayerInfosScreen();

  @override
  _PlayerInfosScreenState createState() => _PlayerInfosScreenState();
}

class _PlayerInfosScreenState extends State<PlayerInfosScreen> {
  final _formKey = GlobalKey<FormState>();

  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();

  bool busyBee = false;

  @override
  Widget build(BuildContext context) {
    Player? player = Provider.of<Player?>(context);
    if (player == null) return Loader();

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          _showForm(player),
          showPrimaryButton(player),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _showForm(Player player) {
    return Form(
        key: _formKey,
        child: Expanded(
          child: ListView(shrinkWrap: true, children: <Widget>[
            showContractInput(player),
            showValueInput(player),
            showSalaryInput(player),
            showAgentInput(player)
          ]),
        ));
  }

  Widget showContractInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Contract'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.contract,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (contract) => player.contract = contract!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showValueInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Market value'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.marketValue,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (marketValue) => player.marketValue = marketValue!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showSalaryInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Salary'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.salary,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (salary) => player.salary = salary!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showAgentInput(Player player) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Agent'.i18n),
        SizedBox(height: 10),
        TextFormField(
            initialValue: player.agent,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(70, 70, 70, 1),
                filled: true,
                border: InputBorder.none),
            cursorColor: Colors.white,
            onSaved: (agent) => player.agent = agent!.trim()),
        SizedBox(height: 30),
      ],
    );
  }

  Widget showPrimaryButton(Player player) {
    return new Padding(
        padding: const EdgeInsets.only(top: 35.0),
        child: UglyButton(
          onPressed: busyBee
              ? null
              : () {
                  _handleSaveForm(player);
                },
          text: 'Save'.i18n,
        ));
  }

  // METHODS

  _handleSaveForm(Player player) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      await updatePlayer(player);
      Navigator.of(context).pop();
    }
  }

  Future<String> updatePlayer(Player player) async {
    try {
      setState(() {
        busyBee = true;
      });

      await Document<Player>(path: 'players/${player.uid}').upsert({
        'contract': player.contract,
        'marketValue': player.marketValue,
        'salary': player.salary,
        'agent': player.agent,
        'flutterUpdate': true
      });

      setState(() {
        busyBee = false;
      });
      return "Player successfully updated".i18n;
    } catch (e) {
      setState(() {
        busyBee = false;
      });

      return _errorHandlerService.toFriendlyMessage(e);
    }
  }
}
