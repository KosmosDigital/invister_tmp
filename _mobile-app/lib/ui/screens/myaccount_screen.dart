import 'package:invister/core/i18n/myaccount.i18n.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/authentication_service.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/better_snackbar.dart';
import 'package:flutter/material.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:provider/provider.dart';

class MyAccountScreen extends StatefulWidget {
  final GlobalKey<ScaffoldMessengerState>? scaffoldKey;

  MyAccountScreen({this.scaffoldKey});

  @override
  _MyAccountScreenState createState() => _MyAccountScreenState();
}

class _MyAccountScreenState extends State<MyAccountScreen> {
  // services
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();
  final NavigationService _navigationService = locator<NavigationService>();

  final _formAccountKey = GlobalKey<FormState>();

  bool busyBee = false;
  bool isNotifOn = false;

  @override
  Widget build(BuildContext context) {
    print("=================> 4");
    //user data
    User? user = Provider.of<User?>(context);

    if (user == null) return Container();

    return Stack(
      children: [
        Container(decoration: BoxDecoration(color: Colors.black)),
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 45.0, right: 45.0),
            child: Column(
              children: [_showTitlePage(), _showAccountForm(user)],
            ),
          ),
        ),
      ],
    );
  }

  Widget _showTitlePage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          "My account".i18n,
          style: TextStyle(
            color: Colors.white,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
        IconButton(
          splashColor: Colors.grey,
          icon: Icon(Icons.exit_to_app),
          onPressed: () {
            showSignoutDialog();
          },
          color: Colors.white,
        ),
      ],
    );
  }

  Widget _showAccountForm(User user) {
    return Form(
      key: _formAccountKey,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          showSurnameInput(user),
          showNameInput(user),
          // showToggleNotifications(user),
          showPrimaryButton(user),
          showAskBox(),
        ],
      ),
    );
  }

  Widget showSurnameInput(User user) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: TextFormField(
        initialValue: user.lastName,
        style: TextStyle(color: Colors.white),
        maxLines: 1,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          enabledBorder: const UnderlineInputBorder(
            borderSide: const BorderSide(
              color: Colors.grey,
              width: 0.0,
            ),
          ),
          hintText: 'Last name'.i18n,
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
        ),
        validator: (value) =>
            value!.isEmpty ? "Last name can't be empty".i18n : null,
        onSaved: (value) => user.lastName = value!.trim(),
      ),
    );
  }

  Widget showNameInput(User user) {
    return TextFormField(
      initialValue: user.firstName,
      style: TextStyle(color: Colors.white),
      maxLines: 1,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'First name'.i18n,
        hintStyle: TextStyle(color: Colors.grey),
        enabledBorder: const UnderlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
        ),
      ),
      validator: (value) =>
          value!.isEmpty ? "First name can't be empty".i18n : null,
      onSaved: (value) => user.firstName = value!.trim(),
    );
  }

  Widget showToggleNotifications(User user) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Notifications'.i18n,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 16.0,
          ),
        ),
        Switch(
          value: user.isNotified!,
          onChanged: (bool value) {
            user.isNotified = value;
            setState(() {});
          },
          activeColor: Colors.blue,
          activeTrackColor: Colors.lightBlueAccent,
          inactiveTrackColor: Colors.white,
        ),
      ],
    );
  }

  Widget showPrimaryButton(User user) {
    return new Padding(
      padding: const EdgeInsets.only(top: 35.0),
      child: UglyButton(
        onPressed: busyBee
            ? null
            : () {
                validateAndSubmitUpdateAccount(user);
              },
        text: 'Confirm'.i18n,
      ),
    );
  }

  Widget showAskBox() {
    return new Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: SizedBox(
        height: 80.0,
        child: new MaterialButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(color: Colors.grey)),
            color: Colors.transparent,
            splashColor: Colors.grey,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
              child: Column(
                children: <Widget>[
                  Text(
                    'Any questions ?'.i18n,
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontStyle: FontStyle.normal),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                    child: Text(
                      'Contact us'.i18n,
                      style: TextStyle(
                          fontSize: 10.0,
                          color: Colors.white,
                          fontStyle: FontStyle.normal),
                    ),
                  )
                ],
              ),
            ),
            onPressed: showContactDialog),
      ),
    );
  }

  Future showContactDialog() {
    return showDialog(
      context: widget.scaffoldKey!.currentContext!,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Contact us'.i18n),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Address'.i18n),
                Text('Phone number'.i18n),
              ],
            ),
          ),
          actions: <Widget>[
            MaterialButton(
              child: Text('Ok'.i18n),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  Future showSignoutDialog() {
    return showDialog(
      context: widget.scaffoldKey!.currentContext!,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Sign out'.i18n),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("You're about to disconnect".i18n),
              ],
            ),
          ),
          actions: <Widget>[
            MaterialButton(
              child: Text('Cancel'.i18n),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            MaterialButton(
              child: Text('Okay'.i18n),
              onPressed: signOut,
            ),
          ],
        );
      },
    );
  }

  // -------------------------- METHODS --------------------------

  void validateAndSubmitUpdateAccount(User user) async {
    if (_formAccountKey.currentState!.validate()) {
      _formAccountKey.currentState!.save();

      String result = await updateUser(user);
      widget.scaffoldKey!.currentState!.showSnackBar(betterSnackBar(result));
    }
  }

  Future<String> updateUser(User user) async {
    try {
      setState(() {
        busyBee = true;
      });

      await UserData<User>(collection: 'users').upsert({
        'firstName': user.firstName,
        'lastName': user.lastName,
        'isNotified': user.isNotified
      });

      setState(() {
        busyBee = false;
      });
      return "User successfully updated".i18n;
    } catch (e) {
      setState(() {
        busyBee = false;
      });

      return _errorHandlerService.toFriendlyMessage(e);
    }
  }

  void signOut() {
    _authenticationService.signOut();
    _navigationService.clearAndNavigateTo(LoginRoute);
  }
}
