import 'package:flutter/material.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/app_colors.dart';
import 'package:invister/ui/shared/loader.dart';
import 'package:invister/ui/shared/player_card.dart';
import 'package:provider/provider.dart';
import 'package:invister/core/i18n/home_screen.i18n.dart';

class HomeScreen extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    print("=================> 3");
    List<Player>? players = Provider.of<List<Player>?>(context);
    User? user = Provider.of<User?>(context);

    print("===============> User22 " + user.toString());

    if (players == null || user == null) return Loader();

    print("=====================> blablabla");

    return Container(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: [
            _showTitlePage(),
            _showPrimaryButton(context, user),
            _showListOfPlayer(players, user),
          ],
        ),
      ),
    );
  }

  Widget _showTitlePage() {
    return Container(
      height: 25,
      color: Colors.black,
      child: Center(
        child: Text(
          'My Players'.i18n,
          style: TextStyle(
              color: Colors.white,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 20.0),
        ),
      ),
    );
  }

  Widget _showPrimaryButton(BuildContext context, User user) {
    return Padding(
        padding: EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 10.0),
        child: SizedBox(
            height: 80.0,
            child: new MaterialButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: Colors.grey)),
              color: Colors.transparent,
              splashColor: Colors.grey,
              child: Container(
                child: Row(
                  children: [
                    Icon(Icons.add_circle, color: PRIMARY_COLOR, size: 35.0),
                    Text(
                      'Add a new player'.i18n,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontStyle: FontStyle.normal),
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                ),
              ),
              onPressed: () => _showNewPlayerDialog(context, user),
            )));
  }

  Widget _showListOfPlayer(List<Player> players, User user) {
    List<Widget> list = [];

    if (user.notifications != null &&
        user.notifications!.containsKey('newPlayers'))
      players.sort((a, b) {
        final newPlayers = user.notifications!['newPlayers'];
        for (var i = 0; i < newPlayers.length; i++) {
          if (a.uid == newPlayers[i])
            return -1;
          else if (b.uid == newPlayers[i]) return 1;
        }

        return 0;
      });

    players.forEach((player) {
      list.add(Padding(
        padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 0.0),
        child: PlayerCard(player: player, isWishlist: false),
      ));
    });

    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, children: list);
  }

  _showNewPlayerDialog(BuildContext context, User user) {
    final _formKey = GlobalKey<FormState>();
    Map<String, String> playerData = Map();

    showDialog(
      context: context,
      builder: (_) => Dialog(
        backgroundColor: Colors.transparent,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.black,
            boxShadow: [
              BoxShadow(
                color: Colors.white,
                blurRadius: 20.0,
              ),
            ],
          ),
          height: 250,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Center(
                  child: Text(
                    "Add Player".i18n,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 24.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextFormField(
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintText: 'Lastname'.i18n,
                              hintStyle: TextStyle(color: Colors.grey)),
                          validator: (value) => value!.isEmpty
                              ? "Lastname can't be empty".i18n
                              : null,
                          onSaved: (value) =>
                              {playerData['lastName'] = value!.trim()},
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          maxLines: 1,
                          style: TextStyle(color: Colors.white),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Firstname'.i18n,
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                          validator: (value) => value!.isEmpty
                              ? "Firstname can't be empty".i18n
                              : null,
                          onSaved: (value) => {
                            playerData['firstName'] = value!.trim(),
                          },
                        ),
                      ],
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    MaterialButton(
                      color: PRIMARY_COLOR,
                      child: Text('Cancel'.i18n),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    MaterialButton(
                      color: PRIMARY_COLOR,
                      child: Text('Save'.i18n),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();
                          Player player =
                              await PlayerData().post(playerData, user);
                          Navigator.of(context).pop();
                          _navigationService.navigateTo(PlayerProfileRoute,
                              arguments: player);
                        }
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
