import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/scheduler.dart';
import 'package:invister/core/services/authentication_service.dart';
import 'package:invister/core/services/globals.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final NavigationService _navService = locator<NavigationService>();
  final AuthenticationService _authService = locator<AuthenticationService>();

  @override
  void initState() {
    super.initState();
    print("================> 5");
    User? user = _authService.getUser;

    SchedulerBinding.instance!.addPostFrameCallback((_) {
      print("================> 6 " + user.toString());
      if (user != null)
        _navService.clearAndNavigateTo(HomeRoute);
      else
        _navService.clearAndNavigateTo(LoginRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Material(
            child: Container(
      decoration: BoxDecoration(
        color: Colors.black,
      ),
      child: Center(
        child: Text(Globals.title),
      ),
    )));
  }
}
