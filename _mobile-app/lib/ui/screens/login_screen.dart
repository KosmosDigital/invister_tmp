import 'package:invister/core/services/authentication_service.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/core/services/globals.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/locator.dart';
import 'package:invister/router.dart';
import 'package:invister/ui/shared/better_snackbar.dart';
import 'package:flutter/material.dart';
import 'package:invister/core/i18n/login.i18n.dart';
import 'package:invister/ui/shared/ugly_button.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen();

  @override
  State<StatefulWidget> createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  final _formLoginKey = new GlobalKey<FormState>();
  final _formResetPassword = new GlobalKey<FormState>();

  final AuthenticationService _authService = locator<AuthenticationService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final ErrorHandlerService _errorHandlerService =
      locator<ErrorHandlerService>();

  String? _email;
  String? _emailResetPassword;
  String? _password;

  bool busyBee = false;

  @override
  void initState() {
    super.initState();
    print("===================> 2");
  }

  final Widget svg = SvgPicture.asset(
    'assets/logo_invister.svg',
    color: Color(0xFFFFFFFF),
  );

  void validateAndSubmitLogIn(BuildContext context) async {
    setState(() {
      busyBee = true;
    });

    if (_formLoginKey.currentState!.validate()) {
      _formLoginKey.currentState!.save();

      try {
        await _authService.loginWithEmail(email: _email, password: _password);
        _navigationService.clearAndNavigateTo(HomeRoute);
      } catch (e) {
        String error = _errorHandlerService.toFriendlyMessage(e);
        ScaffoldMessenger.of(context).showSnackBar(betterSnackBar(error));
      }
    }
    setState(() => busyBee = false);
  }

  void resetPassword(BuildContext context) async {
    if (_formResetPassword.currentState!.validate()) {
      _formResetPassword.currentState!.save();

      try {
        await _authService.resetPassword(_emailResetPassword!);
        ScaffoldMessenger.of(context).showSnackBar(
            betterSnackBar('A reset email has been sent to you'.i18n));
      } catch (e) {
        String error = _errorHandlerService.toFriendlyMessage(e);
        ScaffoldMessenger.of(context).showSnackBar(betterSnackBar(error));
      }
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/background_stadium.png"),
                fit: BoxFit.fitWidth,
                alignment: Alignment.bottomCenter,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 60.0, left: 25.0, right: 25.0),
              child: Column(
                children: <Widget>[
                  Stack(alignment: Alignment.bottomCenter, children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(height: 125.0, width: 300.0, child: svg)
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 58),
                      child: Text(
                        Globals.slogan,
                        style: TextStyle(
                          color: Colors.white,
                          fontStyle: FontStyle.italic,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: 60,
                  ),
                  Text(
                    'Login'.i18n,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Form(
                    key: _formLoginKey,
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        // Email input
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            maxLines: 1,
                            keyboardType: TextInputType.emailAddress,
                            autofocus: false,
                            decoration: InputDecoration(
                              hintText: 'Email'.i18n,
                              hintStyle: TextStyle(color: Colors.grey),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.0),
                              ),
                            ),
                            validator: (value) => value!.isEmpty
                                ? 'Email can\'t be empty'.i18n
                                : null,
                            onSaved: (value) => _email = value!.trim(),
                          ),
                        ),

                        // Password input
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            maxLines: 1,
                            obscureText: true,
                            autofocus: false,
                            decoration: InputDecoration(
                              hintText: 'Password'.i18n,
                              hintStyle: TextStyle(color: Colors.grey),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.0),
                              ),
                            ),
                            validator: (value) => value!.isEmpty
                                ? 'Password can\'t be empty'.i18n
                                : null,
                            onSaved: (value) => _password = value!.trim(),
                          ),
                        ),

                        SizedBox(height: 20),

                        // Forgotten password
                        Container(
                          alignment: Alignment(1, 0),
                          child: MaterialButton(
                            onPressed: () => showResetDialog(context),
                            child: Text('Forgot password'.i18n,
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 12.0)),
                          ),
                        ),

                        // Primary button
                        Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 35.0, 15.0, 0.0),
                          child: UglyButton(
                            text: 'Login'.i18n,
                            onPressed: () => validateAndSubmitLogIn(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Future<dynamic> showResetDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (_) => Dialog(
        backgroundColor: Colors.transparent,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.black,
            boxShadow: [
              BoxShadow(
                color: Colors.white,
                blurRadius: 20.0,
              ),
            ],
          ),
          height: 200,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Enter your email address'.i18n,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0.0),
                  child: Form(
                    key: _formResetPassword,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      maxLines: 1,
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: 'Email'.i18n,
                        hintStyle: TextStyle(color: Colors.grey),
                        enabledBorder: const UnderlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                        ),
                      ),
                      validator: (value) =>
                          value!.isEmpty ? 'Email can\'t be empty'.i18n : null,
                      onSaved: (value) => _emailResetPassword = value!.trim(),
                    ),
                  ),
                ),
                SizedBox(height: 30.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: UglyButton(
                    text: 'Confirm'.i18n,
                    onPressed: () => resetPassword(context),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
