import 'package:invister/core/services/analytics_service.dart';
import 'package:invister/core/services/audioplayer_service.dart';
import 'package:invister/core/services/authentication_service.dart';
import 'package:get_it/get_it.dart';
import 'package:invister/core/services/errorHandler_service.dart';
import 'package:invister/core/services/navigation_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  // service
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => AnalyticsService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => ErrorHandlerService());
  locator.registerLazySingleton(() => AudioPlayerService());
}
