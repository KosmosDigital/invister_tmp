import 'package:firebase_core/firebase_core.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/models/wishlist.dart';
import 'package:invister/core/services/analytics_service.dart';
import 'package:invister/core/services/authentication_service.dart';
import 'package:invister/core/services/db.dart';
import 'package:invister/core/services/globals.dart';
import 'package:invister/core/services/navigation_service.dart';
import 'package:invister/router.dart' as router;
import 'package:flutter/material.dart';
import 'package:invister/locator.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

void main() async {
  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("=============> ");
    return MultiProvider(
      providers: [
        StreamProvider<dynamic>.value(
          value: locator<AuthenticationService>().user,
          initialData: null,
        ),
        StreamProvider<User?>.value(
          value: UserData<User>(collection: 'users').documentStream,
          initialData: null,
        ),
        StreamProvider<List<Wishlist>>.value(
          value: WishlistData().list(),
          initialData: [],
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,

        // title (obviously)
        title: Globals.title,

        //theme
        theme: ThemeData(
            primaryColor: Colors.black,
            scaffoldBackgroundColor: Colors.black,
            textTheme: TextTheme(bodyText2: TextStyle(color: Colors.white))),

        // routing
        initialRoute: router.SplashRoute,
        onGenerateRoute: router.generateRoute,
        navigatorKey: locator<NavigationService>().navigatorKey,
        navigatorObservers: [
          locator<AnalyticsService>().getAnalyticsObserver()
        ],

        // i18n
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],

        supportedLocales: [
          const Locale('en'), // English
          const Locale('fr'), // French
        ],
      ),
    );
  }
}
