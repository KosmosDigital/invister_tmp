import 'package:invister/core/models/player.dart';
import 'package:invister/ui/screens/login_screen.dart';
import 'package:invister/ui/screens/player/evaluations/player_evaluation_screen.dart';
import 'package:invister/ui/screens/player/player_audio_list_screen.dart';
import 'package:invister/ui/screens/player/evaluations/player_evaluation_criterias_screen.dart';
import 'package:invister/ui/screens/player/evaluations/player_evaluation_items_screen.dart';
import 'package:invister/ui/screens/player/player_note_screen.dart';
import 'package:invister/ui/screens/player/player_notes_list_screen.dart';
import 'package:invister/ui/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:invister/ui/screens/wishlist/wishlist_screen.dart';
import 'package:invister/ui/shared/base_scaffold.dart';
import 'package:invister/ui/shared/player_scaffold.dart';

const String LoginRoute = 'Login';
const String SignupRoute = 'SignUp';
const String SplashRoute = 'Splash';
const String HomeRoute = 'Home';
const String WishlistRoute = 'Wishlist';
const String PlayerNotesListRoute = 'PlayerNotesList';
const String PlayerNoteRoute = 'PlayerNote';
const String PlayerAudiosListRoute = 'PlayerAudiosList';
const String PlayerProfileRoute = 'PlayerProfile';
const String PlayerEvaluationRoute = 'PlayerEvaluation';
const String PlayerEvaluationsRoute = 'PlayerEvaluations';
const String PlayerItemsRoute = 'PlayerItems';
const String PlayerCriteriasRoute = 'PlayerCriterias';
const String PlayerClassificationRoute = 'PlayerClassification';
const String PlayerInfosRoute = 'PlayerInfos';
const String PlayerActionsRoute = 'PlayerActions';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {

    // splash
    case SplashRoute:
      return MaterialPageRoute(
          builder: (context) => I18n(child: SplashScreen()));

    // login
    case LoginRoute:
      return MaterialPageRoute(builder: (context) => LoginScreen());

    // base scaffold
    case HomeRoute:
      return MaterialPageRoute(
          builder: (context) => BaseScaffold(selectedIndex: 1));

    // wishlist
    case WishlistRoute:
      Map<String, dynamic> args = settings.arguments as Map<String, dynamic>;
      return MaterialPageRoute(
          builder: (context) => WishlistScreen(
              wishlistUid: args['wishlistUid'], players: args['players']));

    // player

    case PlayerNotesListRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) => PlayerStreamScreen(player: player));
    case PlayerNoteRoute:
      Map<String, dynamic> args = settings.arguments as Map<String, dynamic>;
      return MaterialPageRoute(
          builder: (context) => PlayerNoteScreen(
              uidPlayer: args['uidPlayer'], note: args['note'] ?? null));
    case PlayerAudiosListRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) => PlayerAudiosListScreen(player: player));

    case PlayerItemsRoute:
      Map<String, dynamic> args = settings.arguments as Map<String, dynamic>;
      return MaterialPageRoute(
          builder: (context) => PlayerEvaluationItemsScreen(
              items: args['items'],
              type: args['type'],
              player: args['player'],
              playerEvaluation: args['playerEvaluation']));
    case PlayerCriteriasRoute:
      Map<String, dynamic> args = settings.arguments as Map<String, dynamic>;
      return MaterialPageRoute(
          builder: (context) => PlayerEvaluationCriteriasScreen(
              playerUid: args['playerUid'],
              item: args['item'],
              playerItem: args['playerItem'],
              playerEvaluation: args['playerEvaluation'],
              categoryName: args['categoryName']));

    case PlayerProfileRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) =>
              PlayerScaffold(initialIndex: 0, player: player));
    case PlayerEvaluationsRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) =>
              PlayerScaffold(initialIndex: 1, player: player));
    case PlayerClassificationRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) =>
              PlayerScaffold(initialIndex: 2, player: player));
    case PlayerInfosRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) =>
              PlayerScaffold(initialIndex: 3, player: player));
    case PlayerActionsRoute:
      Player player = settings.arguments as Player;
      return MaterialPageRoute(
          builder: (context) =>
              PlayerScaffold(initialIndex: 4, player: player));
    case PlayerEvaluationRoute:
      Map<String, dynamic> args = settings.arguments as Map<String, dynamic>;
      return MaterialPageRoute(
          builder: (context) => PlayerEvaluationScreen(
              playerEvaluation: args['playerEvaluation'],
              player: args['player']));

    // not found
    default:
      return MaterialPageRoute(
        builder: (context) => Scaffold(
          body: Center(
            child: Text('No path for ${settings.name}'),
          ),
        ),
      );
  }
}
