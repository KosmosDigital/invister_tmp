import 'package:flutter/services.dart';
import 'package:invister/core/i18n/module.i18n.dart';

enum Module { TAC, TEC, PHY, MEN }

extension ModulesExtension on Module {
  String get string {
    switch (this) {
      case Module.TAC:
        return 'Tactique'.i18n;
      case Module.TEC:
        return 'Technique'.i18n;
      case Module.PHY:
        return 'Physique'.i18n;
      case Module.MEN:
        return 'Mental'.i18n;
    }
  }

  String get serialize {
    switch (this) {
      case Module.TAC:
        return 'tactic';
      case Module.TEC:
        return 'technic';
      case Module.PHY:
        return 'physical';
      case Module.MEN:
        return 'mind';
    }
  }
}

extension StringExtension on String {
  Module get deserializeModule {
    switch (this) {
      case 'tactic':
        return Module.TAC;
      case 'technic':
        return Module.TEC;
      case 'physical':
        return Module.PHY;
      case 'mind':
        return Module.MEN;
    }
    throw PlatformException(code: 'ENUM_NOT_FOUND');
  }
}
