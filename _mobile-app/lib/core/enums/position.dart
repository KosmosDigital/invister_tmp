import 'package:flutter/services.dart';
import 'package:invister/core/i18n/position.i18n.dart';

enum Position {
  GK, // GoalKeeper
  LCB, // LeftCenterBack
  RCB, // RightCenterBack
  LB, // LeftBack
  RB, // RightBack
  DM, // Defensive Midfielder
  OM, // Offensive Midfielder
  PM, // Center Midfielder
  LW, // Left Winger
  RW, // Right Winger
  ST, // Striker
}

extension PositionExtension on Position {
  String get string {
    switch (this) {
      case Position.GK:
        return 'Goal'.i18n;
      case Position.LCB:
        return 'Left center back'.i18n;
      case Position.RCB:
        return 'Right center back'.i18n;
      case Position.LB:
        return 'Left back'.i18n;
      case Position.RB:
        return 'Right back'.i18n;
      case Position.DM:
        return 'Defensive midfielder'.i18n;
      case Position.OM:
        return 'Offensive midfielder'.i18n;
      case Position.PM:
        return 'Playmaker'.i18n;
      case Position.LW:
        return 'Left winger'.i18n;
      case Position.RW:
        return 'Right winger'.i18n;
      case Position.ST:
        return 'Striker'.i18n;
    }
  }

  String get shortString {
    switch (this) {
      case Position.GK:
        return 'Goal'.i18n;
      case Position.LCB:
        return 'Left center back'.i18n;
      case Position.RCB:
        return 'Right center back'.i18n;
      case Position.LB:
        return 'Left back'.i18n;
      case Position.RB:
        return 'Right back'.i18n;
      case Position.DM:
        return 'Defensive midfielder'.i18n;
      case Position.OM:
        return 'Offensive midfielder'.i18n;
      case Position.PM:
        return 'Playmaker'.i18n;
      case Position.LW:
        return 'Left winger'.i18n;
      case Position.RW:
        return 'Right winger'.i18n;
      case Position.ST:
        return 'Striker'.i18n;
    }
  }

  String get serialize {
    switch (this) {
      case Position.GK:
        return 'GK';
      case Position.LCB:
        return 'LCB';
      case Position.RCB:
        return 'RCB';
      case Position.LB:
        return 'LB';
      case Position.RB:
        return 'RB';
      case Position.DM:
        return 'DM';
      case Position.OM:
        return 'OM';
      case Position.PM:
        return 'PM';
      case Position.LW:
        return 'LW';
      case Position.RW:
        return 'RW';
      case Position.ST:
        return 'ST';
    }
  }
}

extension StringExtension on String {
  Position get deserializePosition {
    switch (this) {
      case 'ST':
        return Position.ST;
      case 'RW':
        return Position.RW;
      case 'LW':
        return Position.LW;
      case 'PM':
        return Position.PM;
      case 'OM':
        return Position.OM;
      case 'DM':
        return Position.DM;
      case 'RB':
        return Position.RB;
      case 'LB':
        return Position.LB;
      case 'LCB':
        return Position.LCB;
      case 'RCB':
        return Position.RCB;
      case 'GK':
        return Position.GK;
    }
    throw PlatformException(code: 'ENUM_NOT_FOUND');
  }
}
