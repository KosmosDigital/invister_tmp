import 'package:flutter/services.dart';
import 'package:invister/core/i18n/sex.i18n.dart';

enum Sex {
  MALE,
  FEMALE,
}

extension PositionExtension on Sex {
  String get string {
    switch (this) {
      case Sex.MALE:
        return 'Male'.i18n;
      case Sex.FEMALE:
        return 'Female'.i18n;
    }
  }

  String get serialize {
    switch (this) {
      case Sex.MALE:
        return 'male';
      case Sex.FEMALE:
        return 'female';
    }
  }
}

extension StringExtension on String {
  Sex get deserializeSex {
    switch (this) {
      case 'male':
        return Sex.MALE;
      case 'female':
        return Sex.FEMALE;
    }
    throw PlatformException(code: 'ENUM_NOT_FOUND');
  }
}
