import 'package:cloud_firestore/cloud_firestore.dart';

class Audio {
  String? owner;
  String? ownerFullName;
  Timestamp? createdAt;
  String? path;

  Audio({this.createdAt, this.path, this.owner, this.ownerFullName});

  Audio.fromJson(Map<String, dynamic> data) {
    owner = data['owner'] ?? '';
    ownerFullName = data['ownerFullName'] ?? '';
    createdAt = data['createdAt'] ?? Timestamp.now();
    path = data['path'] ?? '';
  }

  Audio.fromAlgolia(Map<String, dynamic> data) {
    owner = data['owner'] ?? '';
    ownerFullName = data['ownerFullName'] ?? '';
    path = data['path'] ?? '';
  }

  Map<String, dynamic> toJSON() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['owner'] = this.owner;
    data['ownerFullName'] = this.ownerFullName;
    data['createdAt'] = this.createdAt;
    data['path'] = this.path;

    return data;
  }
}
