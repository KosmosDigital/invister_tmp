class PlayerCriteria {
  double? grade;
  String? title;
  String? textNote;
  String? audioNote;

  PlayerCriteria({this.grade, this.title, this.textNote, this.audioNote});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['title'] = title;
    data['grade'] = grade;
    data['textNote'] = textNote;
    data['audioNote'] = audioNote;

    return data;
  }
}
