import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:invister/core/models/player_criteria.dart';
import 'package:invister/core/models/player_item.dart';

class PlayerEvaluation {
  String? uid;
  String? ownerName;
  double? grade;

  Timestamp? createdAt;

  // Item List
  List<Category>? categories;

  PlayerEvaluation({this.uid, this.categories});

  PlayerEvaluation.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic>? data = doc.data() as Map<String, dynamic>?;

    uid = doc.id;
    createdAt = data!['createdAt'] ?? Timestamp.now();
    ownerName = data['ownerName'] ?? '';
    grade = data['grade'] != null ? data['grade'].toDouble() : -1;

    List<PlayerItem> tmpTab = [];
    List<PlayerCriteria> tmpTab2 = [];
    List<Category> tmpTab3 = [];
    if (data.containsKey('categories')) {
      for (final c in data['categories']) {
        tmpTab = [];
        if (c.containsKey('items')) {
          for (final i in c['items']) {
            tmpTab2 = [];
            if (i.containsKey('criterias')) {
              for (final c in i['criterias']) {
                if (c['grade'] is int) {
                  c['grade'] = c['grade'].toDouble();
                }
                tmpTab2.add(PlayerCriteria(
                    grade: c['grade'],
                    title: c['title'],
                    textNote: c['textNote'],
                    audioNote: c['audioNote']));
              }
            }
            tmpTab.add(PlayerItem(title: i['title'], criterias: tmpTab2));
          }
        }
        tmpTab3.add(Category(type: c['type'], items: tmpTab));
      }
    }
    categories = tmpTab3;

    categories!.forEach((Category category) {});
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    List<Map<String, dynamic>> cat = [];
    this.categories!.forEach((c) {
      cat.add(c.toJson());
    });

    data['categories'] = cat;
    data['ownerName'] = ownerName;
    data['createdAt'] = createdAt;
    data['grade'] = grade;

    return data;
  }
}

class Category {
  String? type;
  List<PlayerItem>? items;

  Category({this.type, this.items});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    List<Map<String, dynamic>> it = [];
    this.items!.forEach((i) {
      it.add(i.toJson());
    });

    data['type'] = type;
    data['items'] = it;

    return data;
  }
}
