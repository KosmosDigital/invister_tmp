import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  String? uid;
  String? firstName;
  String? lastName;
  String? parent;
  String? email;
  bool? isNotified;
  bool? isApproved;
  Map<String, dynamic>? notifications;

  User({
    this.uid,
    this.firstName,
    this.lastName,
    this.isNotified,
    this.parent,
    this.notifications,
    this.email,
    this.isApproved,
  });

  User.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic>? data = doc.data() as Map<String, dynamic>?;

    print("=====================> " + data.toString());

    uid = doc.id;
    firstName = data!['firstName'];
    lastName = data['lastName'];
    isNotified = data['isNotified'];
    parent = data['parent'];
    email = data['email'];
    isApproved = data['approved'];
    notifications = data['notifications'];
  }
}
