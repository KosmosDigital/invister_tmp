import 'package:cloud_firestore/cloud_firestore.dart';

class PlayerClassification {
  String? uid;
  Map<String, String>? classifications;

  PlayerClassification({
    this.uid,
    this.classifications,
  });

  PlayerClassification.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic>? data = doc.data() as Map<String, dynamic>?;

    uid = doc.id;

    classifications = data != null && data.containsKey('classifications')
        ? Map<String, String>.from(data['classifications'])
        : Map<String, String>();
  }
}
