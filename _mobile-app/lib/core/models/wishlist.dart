import 'package:cloud_firestore/cloud_firestore.dart';

class Wishlist {
  String? uid;
  String? name;
  String? description;
  List<String>? players;
  String? user;

  Wishlist({this.uid, this.name, this.description, this.players, this.user});

  Wishlist.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic>? data = doc.data() as Map<String, dynamic>?;

    uid = doc.id;
    name = data!['name'];
    description = data['description'];
    players =
        data.containsKey('players') ? List<String>.from(data['players']) : [];
    user = data['user'];
  }
}
