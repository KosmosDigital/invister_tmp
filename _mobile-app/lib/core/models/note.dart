import 'package:cloud_firestore/cloud_firestore.dart';

class Note {
  String? uid;
  String? parent;
  String? ownerName;

  Timestamp? createdAt;
  String? text;

  Note({this.uid, this.parent, this.createdAt, this.text});

  Note.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

    uid = doc.id;
    parent = data['parent'] ?? '';

    createdAt = data['createdAt'] ?? Timestamp.now();
    ownerName = data['ownerName'] ?? '';
    text = data['text'] ?? '';
  }
}
