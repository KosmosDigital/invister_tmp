import 'package:algolia/algolia.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:invister/core/enums/sex.dart';
import 'package:invister/core/models/audio.dart';

class Player {
  String? uid;
  String? owner;

  // BIO
  String? avatar;
  String? firstName;
  String? lastName;
  Sex? sex;
  String? position;
  DateTime? dateOfBirth;
  String? placeOfBirth;
  String? club;
  String? championship;
  String? size;
  String? weight;
  String? strongFoot;
  String? citizenship;
  String? injuries;

  // INFOS
  String? contract;
  String? marketValue;
  String? salary;
  String? agent;

  // ACTIONS
  bool? requestForTransfer;
  bool? requestForLoan;
  bool? isUrgent;
  bool? fileReport;
  bool? toWatch;

  // VARIOUS
  double? grade;

  // AUDIO
  List<Audio>? audioNotes;

  Player({
    this.uid,
    this.owner,
    this.avatar,
    this.firstName,
    this.lastName,
    this.sex,
    this.position,
    this.dateOfBirth,
    this.placeOfBirth,
    this.club,
    this.championship,
    this.size,
    this.weight,
    this.strongFoot,
    this.citizenship,
    this.injuries,
    this.contract,
    this.marketValue,
    this.salary,
    this.agent,
    this.requestForTransfer,
    this.requestForLoan,
    this.isUrgent,
    this.fileReport,
    this.toWatch,
    this.grade,
    this.audioNotes,
  });

  Player.fromFirestore(DocumentSnapshot? doc) {
    Map<String, dynamic>? data = doc!.data() as Map<String, dynamic>?;

    // BIO

    uid = doc.id;
    owner = data!['owner'] ?? '';

    avatar = data['avatar'];
    firstName = data['firstName'] ?? '';
    lastName = data['lastName'] ?? '';
    sex =
        data['sex'] != null ? data['sex'].toString().deserializeSex : Sex.MALE;

    position = data['position'] ?? '';

    dateOfBirth = data['dateOfBirth'] != null
        ? data['dateOfBirth'].toDate()
        : DateTime.now();
    placeOfBirth = data['placeOfBirth'] ?? '';
    club = data['club'] ?? '';
    championship = data['championship'] ?? '';
    size = data['size'] ?? '';
    weight = data['weight'] ?? '';
    strongFoot = data['strongFoot'] ?? '';
    citizenship = data['citizenship'] ?? '';
    injuries = data['injuries'] ?? '';

    // INFOS

    contract = data['contract'] ?? '';
    marketValue = data['marketValue'] ?? '';
    salary = data['salary'] ?? '';
    agent = data['agent'] ?? '';

    // ACTIONS

    requestForLoan = data['requestForLoan'] ?? false;
    requestForTransfer = data['requestForTransfer'] ?? false;
    isUrgent = data['isUrgent'] ?? false;
    fileReport = data['fileReport'] ?? false;
    toWatch = data['toWatch'] ?? false;

    // VARIOUS

    grade = data['grade'] != null ? data['grade'].toDouble() : -1;

    // AUDIO

    audioNotes = data.containsKey('audioNotes')
        ? List<Audio>.from(
            data['audioNotes'].map((aN) => Audio.fromJson(aN)).toList())
        : [];
  }

  Player.fromAlgolia(AlgoliaObjectSnapshot doc) {
    Map<String, dynamic> data = doc.data;

    // BIO

    uid = doc.objectID;

    avatar = data['avatar'];
    firstName = data['firstName'] ?? '';
    lastName = data['lastName'] ?? '';
    sex =
        data['sex'] != null ? data['sex'].toString().deserializeSex : Sex.MALE;

    position = data['position'] ?? '';

    dateOfBirth = DateTime.now();
    placeOfBirth = data['placeOfBirth'] ?? '';
    club = data['club'] ?? '';
    championship = data['championship'] ?? '';
    size = data['size'] ?? '';
    weight = data['weight'] ?? '';
    strongFoot = data['strongFoot'] ?? '';
    citizenship = data['citizenship'] ?? '';
    injuries = data['injuries'] ?? '';

    // INFOS

    contract = data['contract'] ?? '';
    marketValue = data['marketValue'] ?? '';
    salary = data['salary'] ?? '';
    agent = data['agent'] ?? '';

    // ACTIONS

    requestForLoan = data['requestForLoan'] ?? false;
    requestForTransfer = data['requestForTransfer'] ?? false;
    isUrgent = data['isUrgent'] ?? false;
    fileReport = data['fileReport'] ?? false;
    toWatch = data['toWatch'] ?? false;

    // VARIOUS

    grade = data['grade'] != null ? data['grade'].toDouble() : -1;

    // AUDIO

    audioNotes = data.containsKey('audioNotes')
        ? List<Audio>.from(
            data['audioNotes'].map((aN) => Audio.fromAlgolia(aN)).toList())
        : [];
  }
}
