import 'package:invister/core/models/player_criteria.dart';

class PlayerItem {
  String? title;
  List<PlayerCriteria>? criterias;

  PlayerItem({this.title, this.criterias});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    List<Map<String, dynamic>> crit = [];
    this.criterias!.forEach((c) {
      crit.add(c.toJson());
    });

    data['title'] = title;
    data['criterias'] = crit;

    return data;
  }
}
