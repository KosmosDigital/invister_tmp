import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:invister/core/models/item.dart';

class Methodology {
  String? uid;

  // Title
  String? name;

  // Owner
  String? owner;

  // Evaluation
  List<Item>? athletic;
  List<Item>? mind;
  List<Item>? tactic;
  List<Item>? technic;

  // Classifications
  List<Item>? classifications;

  Methodology(
      {this.uid,
      this.name,
      this.owner,
      this.athletic,
      this.mind,
      this.tactic,
      this.technic,
      this.classifications});

  Methodology.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

    uid = doc.id;
    owner = data['owner'] ?? '';
    name = data['name'] ?? '';

    athletic = data.containsKey('athletic')
        ? List<Item>.from(data['athletic'].map((item) => Item(
            title: item['title'],
            criterias: List<String>.from(
                item['criteria'].map((criterion) => criterion as String)))))
        : [];

    tactic = data.containsKey('tactic')
        ? List<Item>.from(data['tactic'].map((item) => Item(
            title: item['title'],
            criterias: List<String>.from(
                item['criteria'].map((criterion) => criterion as String)))))
        : [];

    technic = data.containsKey('technic')
        ? List<Item>.from(data['technic'].map((item) => Item(
            title: item['title'],
            criterias: List<String>.from(
                item['criteria'].map((criterion) => criterion as String)))))
        : [];

    mind = data.containsKey('mind')
        ? List<Item>.from(data['mind'].map((item) => Item(
            title: item['title'],
            criterias: List<String>.from(
                item['criteria'].map((criterion) => criterion as String)))))
        : [];

    classifications = data.containsKey('classification')
        ? List<Item>.from(data['classification'].map((item) => Item(
            title: item['title'],
            criterias: List<String>.from(
                item['criteria'].map((criterion) => criterion as String)))))
        : [];
  }
}
