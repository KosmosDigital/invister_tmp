import 'package:invister/core/models/player_evaluation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PlayerEvaluations {
  String? uid;

  List<PlayerEvaluation>? evaluation;

  PlayerEvaluations.fromFirestore(DocumentSnapshot doc) {
    uid = doc.id;
  }
}
