import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "Se connecter",
      "en": "Login",
    } +
    {
      "fr": "Email",
      "en": "Email",
    } +
    {
      "fr": "L\'email ne peut pas être nul",
      "en": "Email can\'t be empty",
    } +
    {
      "fr": "Mot de passe",
      "en": "Password",
    } +
    {
      "fr": "Le mot de passe ne peut pas être nul",
      "en": "Password can\'t be empty",
    } +
    {
      "fr": "Valider",
      "en": "Confirm",
    } +
    {
      "fr": "Mot de passe oublié",
      "en": "Forgot password",
    } +
    {
      "fr": "Un email de réinitialisation vous a été envoyé",
      "en": "A reset email has been sent to you",
    } +
    {
      "fr": "Entrez votre adresse email:",
      "en": "Enter your email address",
    };

String get i18n => localize(this, t);
}
