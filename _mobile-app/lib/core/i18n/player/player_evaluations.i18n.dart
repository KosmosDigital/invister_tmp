import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "  Invister",
        "en": "  Invister",
      } +
      {
        "fr": "Nouvelle Evaluation",
        "en": "New Evaluation",
      } +
      {
        "fr": "Enregistrement...",
        "en": "Recording...",
      } +
      {
        "fr": "Suivi par: ",
        "en": "Follow by: ",
      } +
      {
        "fr": "Evaluation du ",
        "en": 'Evaluation of ',
      } +
      {
        "fr": " - Par ",
        "en": ' - By ',
      } +
      {
        "fr": "Voulez vous supprimer l'évaluation ?",
        "en": 'Do you want to delete this evaluation ?',
      } +
      {
        "fr": "Suppression de l'évaluation",
        "en": 'Delete evaluation',
      } +
      {
        "fr": "Confirmer",
        "en": 'Confirm',
      } +
      {
        "fr": "Annuler",
        "en": 'Cancel',
      };

  String get i18n => localize(this, t);
}
