import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "Rapport du ",
        "en": 'Report of ',
      } +
      {
        "fr": " - Par ",
        "en": " - By ",
      } +
      {
        "fr": "Voulez vous supprimer le rapport audio ?",
        "en": 'Do you want to delete this audio note ?',
      } +
      {
        "fr": "Suppression du rapport audio'",
        "en": 'Delete audio note',
      } +
      {
        "fr": "Confirmer",
        "en": 'Confirm',
      } +
      {
        "fr": "Annuler",
        "en": 'Cancel',
      };

  String get i18n => localize(this, t);
}
