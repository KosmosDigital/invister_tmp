import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "Nom",
        "en": "Lastname",
      } +
      {
        "fr": "Prénom",
        "en": "Firstname",
      } +
      {
        "fr": "Sexe",
        "en": "Sex",
      } +
      {
        "fr": "Poste",
        "en": "Position",
      } +
      {
        "fr": "Date de naissance",
        "en": "Date of birth",
      } +
      {
        "fr": "Lieu de naissance",
        "en": "Place of birth",
      } +
      {
        "fr": "Club",
        "en": "Club",
      } +
      {
        "fr": "Championnat",
        "en": "Championship",
      } +
      {
        "fr": "Taille",
        "en": "Size",
      } +
      {
        "fr": "Poids",
        "en": "Weight",
      } +
      {
        "fr": "Pied fort",
        "en": "Strong foot",
      } +
      {
        "fr": "Nationnalité",
        "en": "Citizenship",
      } +
      {
        "fr": "Blessures",
        "en": "Injuries",
      } +
      {
        "fr": "Enregistrer",
        "en": "Save",
      } +
      {
        "fr": "Télécharger une image",
        "en": "Upload a new image",
      } +
      {
        "fr": "Joueur mise à jour avec succès",
        "en": "Player successfully updated"
      };

  String get i18n => localize(this, t);
}
