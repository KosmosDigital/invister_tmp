import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": 'Chargement...',
      "en": "Loading...",
    } +
    {
      "fr": "Erreur",
      "en": "Error",
    };

String get i18n => localize(this, t);
}
