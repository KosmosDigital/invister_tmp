import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "Assignez un poste au joueur",
        "en": 'Set a position for the player first',
      } +
      {
        "fr": "La classification est vide",
        "en": 'The classification is empty',
      };

  String get i18n => localize(this, t);
}
