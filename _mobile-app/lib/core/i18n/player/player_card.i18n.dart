import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "ans",
        "en": " years old",
      } +
      {
        "fr": "Rapport",
        "en": "Report",
      } +
      {
        "fr": "Audio",
        "en": "Audio",
      } +
      {
        "fr": "Évaluation",
        "en": "Evaluation",
      } +
      {
        "fr": "Confirmer",
        "en": "Confirm",
      } +
      {
        "fr": "Vous n\'avez pas encore de liste",
        "en": 'You don\'t have any wishlist yet',
      } +
      {
        "fr":
            "Vous êtes sur le point de supprimer ce joueur. Si vous voulez juste le retirer de la liste, appuyez sur l'étoile",
        "en":
            'You are about to delete this player. If you only want to remove it from a wishlist, just tap on the star.',
      } +
      {
        "fr": "Vous êtes sur le point de supprimer ce joueur. Êtes-vous sûr ?",
        "en": "You are about to delete this player. Are you sure ?",
      } +
      {
        "fr": "Choisir une liste",
        "en": "Select a wishlist",
      };

  String get i18n => localize(this, t);
}
