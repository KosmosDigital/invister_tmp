import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "Contrat",
        "en": "Contract",
      } +
      {
        "fr": "Valeur marchande",
        "en": "Market value",
      } +
      {
        "fr": "Salaire",
        "en": "Salary",
      } +
      {
        "fr": "Agent",
        "en": "Agent",
      } +
      {
        "fr": "Enregistrer",
        "en": "Save",
      } +
      {
        "fr": "Joueur mise à jour avec succès",
        "en": "Player successfully updated",
      };

  String get i18n => localize(this, t);
}
