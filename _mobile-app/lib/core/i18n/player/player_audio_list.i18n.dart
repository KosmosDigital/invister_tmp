import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "  Invister",
        "en": "  Invister",
      } +
      {
        "fr": "Faire un rapport audio",
        "en": "Add a new audio note",
      } +
      {
        "fr": "Enregistrement...",
        "en": "Recording...",
      } +
      {
        "fr": "Téléchargement...",
        "en": "Uploading...",
      } +
      {
        "fr": "Terminé!",
        "en": "Done!",
      } +
      {
        "fr": "Suivi par: ",
        "en": "Follow by: ",
      };

  String get i18n => localize(this, t);
}
