import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": " Invister",
        "en": " Invister",
      } +
      {
        "fr": "Un rapport ne peut pas être vide",
        "en": "A note can't be empty",
      } +
      {
        "fr": "Enregistrer",
        "en": "Save",
      } +
      {
        "fr": "Rapport créé",
        "en": "Note created",
      } +
      {
        "fr": "Rapport mis à jour",
        "en": "Note updated",
      };

  String get i18n => localize(this, t);
}
