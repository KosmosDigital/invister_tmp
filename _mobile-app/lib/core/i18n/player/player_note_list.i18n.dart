import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "  Invister",
      "en": "  Invister",
    } +
    {
      "fr": "Ajouter une nouvelle note",
      "en": 'Add a new note',
    } +
    {
      "fr": "Suivi par: ",
      "en": "Follow by: ",
    } +
    {
      "fr": "Rapport du ",
      "en": 'Rapport of ',
    } +
    {
      "fr": " - Par ",
      "en": ' - By ',
    } +
    {
      "fr": "Voulez vous supprimer la note ?",
      "en": 'Do you want to delete this note ?',
    } +
    {
      "fr": "Suppression de la note",
      "en": 'Delete note',
    } +
    {
      "fr": "Confirmer",
      "en": 'Confirm',
    } +
    {
      "fr": "Annuler",
      "en": 'Cancel',
    };

String get i18n => localize(this, t);
}
