import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "Demander un transfert",
      "en": "Request a transfer",
    } +
    {
      "fr": "Joueur mise à jour avec succès",
      "en": "Player successfully updated",
    } +
    {
      "fr": "Demander un prêt",
      "en": "Request a loan",
    } +
    {
      "fr": "Urgent",
      "en": "Urgent",
    } +
    {
      "fr": "Faire un rapport",
      "en": "File a report",
    } +
    {
      "fr": "À observer",
      "en": "To watch",
    } +
    {
      "fr": "Enregistrer",
      "en": "Save",
    };

String get i18n => localize(this, t);
}
