import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": '  Invister',
      "en": "  Invister",
    } +
    {
      "fr": "Profil",
      "en": "Profile",
    } +
    {
      "fr": "Évaluation",
      "en": 'Evaluation',
    } +
    {
      "fr": "Infos",
      "en": "Infos",
    } +
    {
      "fr": "Actions",
      "en": "Actions",
    } +
    {
      "fr": "Classification",
      "en": "Classification",
    };

String get i18n => localize(this, t);
}
