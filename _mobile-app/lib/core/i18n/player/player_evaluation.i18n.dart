import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var t = Translations("en") +
      {
        "fr": "TACTIQUE",
        "en": "TACTIC",
      } +
      {
        "fr": "Aucune méthodologies trouvées",
        "en": 'No methodologies found',
      } +
      {
        "fr": "Aucun poste pour ce joueur",
        "en": "Select a position first"
      } +
      {
        "fr": "Aucune évaluation trouvée",
        "en": 'No base evaluation found',
      } +
      {
        "fr": "Aucune méthodologie trouvée pour",
        "en": 'No methodology found for ',
      } +
      {
        "fr": "TECHNIQUE",
        "en": "TECHNIC",
      } +
      {
        "fr": "ATHLETIQUE",
        "en": "ATHLETIC",
      } +
      {
        "fr": "MENTAL",
        "en": "MIND",
      };

  String get i18n => localize(this, t);
}
