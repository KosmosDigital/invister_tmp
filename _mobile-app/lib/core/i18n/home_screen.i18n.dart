import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var t = Translations("en") +
        {
          "fr": "Mes joueurs",
          "en": "My Players",
        } +
        {
          "fr": "Ajouter un joueur",
          "en": "Add a new player",
        } +
        {
          "fr": "Ajouter un joueur",
          "en": "Add Player",
        } +
        {
          "fr": "Nom",
          "en": "Lastname",
        } +
        {
          "fr": "Le nom ne peut pas être vide",
          "en": "Lastname can't be empty",
        } +
        {
          "fr": "Le prénom ne peut pas être vide",
          "en": "Firstname can't be empty",
        } +
        {
          "fr": "Annuler",
          "en": "Cancel",
        } +
        {
          "fr": "Enregistrer",
          "en": "Save",
        } +
        {
          "fr": "Prénom",
          "en": "Firstname",
        };

  String get i18n => localize(this, t);
}