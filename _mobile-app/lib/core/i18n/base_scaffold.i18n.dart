import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "  DEVIENS PRO",
      "en": "  BECOME A PRO",
    };

String get i18n => localize(this, t);
}
