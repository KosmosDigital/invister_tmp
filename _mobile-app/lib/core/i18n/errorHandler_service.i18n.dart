import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
static var t = Translations("en") +
    {
      "fr": "Adresse email invalide.",
      "en": "Your email address appears to be malformed.",
    } +
    {
      "fr": "Mot de passe erroné.",
      "en": "Your password is wrong.",
    } +
    {
      "fr": "Aucun utilisateur enregistré avec cette adresse.",
      "en": "User with this email doesn't exist.",
    } +
    {
      "fr": "Votre compte a été désactivé.",
      "en": "User with this email has been disabled.",
    } +
    {
      "fr":
      "Nombre maximum de tentatives atteint. Veuillez réessayer plus tard.",
      "en": "Too many requests. Try again later.",
    } +
    {
      "fr": "L'authentification par email/mot de passe est désactivée.",
      "en": "Signing in with Email and Password is not enabled.",
    } +
    {
      "fr": "Adresse email déjà utilisée.",
      "en": "Your email address appears to be already used.",
    } +
    {
      "fr": "Votre mot de passe doit contenir au moins 6 caractères.",
      "en": "Your password must contain at least 6 characters.",
    } +
    {
      "fr": "L'inscription a échoué.",
      "en": "Sign up failed.",
    } +
    {
      "fr": "La connexion a échoué.",
      "en": "Login failed.",
    } +
    {
      "fr": "Utilisateur introuvable.",
      "en": "Could not retrieve user.",
    } +
    {
      "fr": "Erreur réseau. Impossible de se connecter à la plateforme.",
      "en":
      "A network error (such as timeout, interrupted connection or unreachable host) has occurred.",
    } +
    {
      "fr": "Ce type n'existe pas.",
      "en": "This enum type does not exist.",
    } +
    {
      "fr": "Aucune autorisation d'enregistrement n'est accordée.",
      "en": "No recording permission given.",
    } +
    {
      "fr": "Erreur inconnue.",
      "en": "Unknown error.",
    };

String get i18n => localize(this, t);
}
