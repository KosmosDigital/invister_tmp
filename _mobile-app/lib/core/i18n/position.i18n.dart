import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var t = Translations("en") +
        {
          "fr": "Gardien",
          "en": "Goal",
        } +
        {
          "fr": "Défenseur central gauche",
          "en": "Left center back",
        } +
        {
          "fr": "Défenseur central droit",
          "en": "Right center back",
        } +
        {
          "fr": "Déf. central gauche",
          "en": "Left center back",
        } +
        {
          "fr": "Déf. central droit",
          "en": "Right center back",
        } +
        {
          "fr": "Défenseur latéral gauche",
          "en": "Left back",
        } +
        {
          "fr": "Défenseur latéral droit",
          "en": "Right back",
        } +
        {
          "fr": "Latéral gauche",
          "en": "Left back",
        } +
        {
          "fr": "Latéral droit",
          "en": "Right back",
        } +
        {
          "fr": "Milieu défensif",
          "en": "Defensive midfielder",
        } +
        {
          "fr": "Milieu offensif",
          "en": "Offensive midfielder",
        } +
        {
          "fr": "Milieu de terrain défensif",
          "en": "Defensive  midfielder",
        } +
        {
          "fr": "Milieu de terrain offensif",
          "en": "Offensive  midfielder",
        } +
        {
          "fr": "Meneur de jeu",
          "en": "Playmaker",
        } +
        {
          "fr": "Milieu gauche",
          "en": "Left winger",
        } +
        {
          "fr": "Milieu droit",
          "en": "Right winger",
        } +
        {
          "fr": "Attaquant",
          "en": "Striker",
        };

  String get i18n => localize(this, t);
}