import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "Mes favoris",
      "en": 'Your Wishlists',
    } +
    {
      "fr": "Ajouter une liste",
      "en": "Add a new wishlist",
    } +
    {
      "fr": "Créer la liste dans laquelle vous souhaitez ajouter des joueurs.",
      "en": 'Create the list you want to add players to.',
    } +
    {
      "fr": "Le nom ne peut pas être vide",
      "en": "Name can't be empty",
    } +
    {
      "fr": "Description",
      "en": "Description",
    } +
    {
      "fr": "Enregistrer",
      "en": "Save",
    } +
    {
      "fr": "Annuler",
      "en": "Cancel",
    } +
    {
      "fr": "Nom",
      "en": "Name",
    } +
    {
      "fr": "La description ne peut pas être vide",
      "en": "Description can't be empty",
    };

String get i18n => localize(this, t);
}
