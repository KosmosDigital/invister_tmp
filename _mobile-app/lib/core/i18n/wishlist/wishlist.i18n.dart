import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "INVISTER",
      "en": 'INVISTER',
    } +
    {
      "fr": "Mes favoris",
      "en": "Liked Players",
    };

String get i18n => localize(this, t);
}
