import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

static var t = Translations("en") +
    {
      "fr": "Mon compte",
      "en": "My account",
    } +
    {
      "fr": "Nom",
      "en": "Last name",
    } +
    {
      "fr": "Le nom ne peut pas être nul",
      "en": "Last name can't be empty",
    } +
    {
      "fr": "Prénom",
      "en": "First name",
    } +
    {
      "fr": "Le prénom de ne peut pas être nul",
      "en": "First name can't be empty",
    } +
    {
      "fr": "Notifications",
      "en": "Notifications",
    } +
    {
      "fr": "Valider",
      "en": "Confirm",
    } +
    {
      "fr": "Une question ?",
      "en": "Any questions ?",
    } +
    {
      "fr": "Contactez-nous",
      "en": "Contact us",
    } +
    {
      "fr": "Adresse",
      "en": "Address",
    } +
    {
      "fr": "Téléphone",
      "en": "Phone number",
    } +
    {
      "fr": "Ok",
      "en": "Ok",
    } +
    {
      "fr": "Déconnexion",
      "en": "Sign out",
    } +
    {
      "fr": "Vous êtes sur le point de vous déconnecter.",
      "en": "You're about to disconnect",
    } +
    {
      "fr": "Annuler",
      "en": "Cancel",
    } +
    {
      "fr": "D\'accord",
      "en": "Okay",
    } +
    {
      "fr": "Utilisateur mise à jour avec succès",
      "en": "User successfully updated",
    };

String get i18n => localize(this, t);
}
