import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var t = Translations("en") +
        {
          "fr": "Tactique",
          "en": "Tactic",
        } +
        {
          "fr": "Technique",
          "en": "Technic",
        } +
        {
          "fr": "Physique",
          "en": "Physical",
        } +
        {
          "fr": "Mental",
          "en": "Mind",
        };

  String get i18n => localize(this, t);
}