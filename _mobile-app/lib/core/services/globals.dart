import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/note.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_classification.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/wishlist.dart';

import '../models/user.dart';

/// Static global state. Immutable services that do not care about build context.
class Globals {
  static final String title = 'Invister';
  static final String slogan = 'working for tomorrow';

  // Data Models
  static final Map models = {
    User: (data) => User.fromFirestore(data),
    Player: (data) => Player.fromFirestore(data),
    Note: (data) => Note.fromFirestore(data),
    Wishlist: (data) => Wishlist.fromFirestore(data),
    PlayerEvaluation: (data) => PlayerEvaluation.fromFirestore(data),
    PlayerClassification: (data) => PlayerClassification.fromFirestore(data),
    Methodology: (data) => Methodology.fromFirestore(data),
  };
}
