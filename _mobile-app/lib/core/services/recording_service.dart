import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:invister/core/models/audio.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/services/db.dart';
import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:record/record.dart';
import 'package:uuid/uuid.dart';

class RecordingService {
  static String? recordingPath;

  Future<void> start() async {
    bool hasPermission = await Record().hasPermission();
    if (!hasPermission)
      throw PlatformException(code: "NO_RECORDING_PERMISSION");

    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;

    Uuid uuid = Uuid();
    String fileName = uuid.v4();

    recordingPath = "$tempPath/$fileName.m4a";

    await Record().start(
      path: recordingPath,
      encoder: AudioEncoder.AAC,
      bitRate: 128000,
      samplingRate: 44100,
    );
  }

  Future<Duration?> getCurrentDuration() async {
    return await AudioPlayer().setFilePath(recordingPath!);
  }

  Future<String?> stop() {
    return Record().stop();
  }

  Future<void> uploadToPlayer(Player player, User me) async {
    final audioFile = File(recordingPath!);
    final fileName = audioFile.path.split('/').last;

    Reference ref = FirebaseStorage.instance.ref().child('audios/' + fileName);
    UploadTask uploadTask = ref.putFile(audioFile);

    await uploadTask;

    player.audioNotes!.add(
      Audio(
          path: fileName,
          createdAt: Timestamp.now(),
          owner: me.uid,
          ownerFullName: '${me.firstName} ${me.lastName}'),
    );

    List<Map<String, dynamic>> audioNotes =
        player.audioNotes!.map((aN) => aN.toJSON()).toList();

    await Document<Player>(path: "players/${player.uid}").upsert({
      'audioNotes': audioNotes,
    });
  }

  Future<String> uploadToPlayerEvaluation(
      PlayerEvaluation playerEvaluation) async {
    final audioFile = File(recordingPath!);
    final fileName = audioFile.path.split('/').last;

    Reference ref = FirebaseStorage.instance.ref().child('audios/' + fileName);
    UploadTask uploadTask = ref.putFile(audioFile);

    await uploadTask;

    return fileName;
  }
}
