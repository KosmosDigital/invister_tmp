import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:invister/core/models/audio.dart';

class AudioPlayerService {
  AudioPlayer audioPlayer = AudioPlayer();

  Future<Stream<void>> play(Audio audio) async {
    Reference ref =
        FirebaseStorage.instance.ref().child('audios/${audio.path}');
    String url = await ref.getDownloadURL();

    audioPlayer.play(url);

    return audioPlayer.onPlayerCompletion;
  }

  Future<Stream<void>> playFromPath(String path) async {
    Reference ref = FirebaseStorage.instance.ref().child('audios/$path');
    String url = await ref.getDownloadURL();

    audioPlayer.play(url);

    return audioPlayer.onPlayerCompletion;
  }

  void pause() {
    audioPlayer.pause();
  }

  void stop() {
    audioPlayer.release();
  }

  void resume() {
    audioPlayer.resume();
  }
}
