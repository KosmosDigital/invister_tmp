import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:invister/core/models/methodology.dart';
import 'package:invister/core/models/note.dart';
import 'package:invister/core/models/player.dart';
import 'package:invister/core/models/player_evaluation.dart';
import 'package:invister/core/models/player_evaluations.dart';
import 'package:invister/core/models/user.dart';
import 'package:invister/core/models/wishlist.dart';
import 'dart:async';
import 'package:rxdart/rxdart.dart';
import './globals.dart';

class Document<T> {
  final FirebaseFirestore? _db = FirebaseFirestore.instance;
  final String? path;

  Document({this.path});

  Future<T> get() {
    return _db!.doc(path!).get().then((v) => Globals.models[T](v) as T);
  }

  Stream<T> streamData() {
    return _db!.doc(path!).snapshots().map((v) {
      print("============================> 9 ");
      return Globals.models[T](v);
    });
  }

  Future<void> upsert(Map data) {
    return _db!.doc(path!).set(
        Map<String, dynamic>.from(data),
        SetOptions(
          merge: true,
        ));
  }
}

class Collection<T> {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final String? path;

  Collection({this.path});

  Future<List<T>> get() async {
    var snapshots = await _db.collection(path!).get();
    return snapshots.docs.map((doc) => Globals.models[T](doc) as T).toList();
  }

  Stream<List<T>> streamData() {
    return _db.collection(path!).snapshots().map(
        (list) => list.docs.map((doc) => Globals.models[T](doc) as T).toList());
  }
}

class UserData<T> {
  final auth.FirebaseAuth? _auth = auth.FirebaseAuth.instance;
  final String? collection;

  UserData({this.collection});

  Stream<T?> get documentStream {
    print("======================> lol");

    return _auth!.authStateChanges().switchMap<T?>((user) {
      print("======================> User " + user.toString());
      if (user != null) {
        Document<T> doc = Document<T>(path: '$collection/${user.uid}');
        return doc.streamData();
      } else {
        return Stream<T?>.value(null);
      }
    });
  }

  Future<dynamic> get() async {
    auth.User? user = _auth!.currentUser;

    if (user != null) {
      Document? doc = Document<T>(path: '$collection/${user.uid}');
      return doc.get();
    } else {
      return null;
    }
  }

  Future<void> upsert(Map data) async {
    auth.User? user = _auth!.currentUser;
    Document<T> ref = Document(path: '$collection/${user!.uid}');
    return ref.upsert(data);
  }
}

class MethodologyData {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  Stream<List<Methodology>> list(String hsxId) {
    return _db
        .collection('methodologies')
        .where('owner', isEqualTo: hsxId)
        .snapshots()
        .map((list) =>
            list.docs.map((doc) => Methodology.fromFirestore(doc)).toList());
  }
}

class PlayerData {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  Future<Player> post(Map<String, dynamic> data, User user) async {
    data['owner'] =
        user.parent == "none" ? user.uid : user.parent! + '_' + user.uid!;

    DocumentReference doc = await _db.collection('players').add(data);

    return doc.get().then((v) => Globals.models[Player](v) as Player);
  }

  Stream<List<Player>> list(String treePath) {
    print("================> List");
    return _db
        .collection('players')
        .where('owner', isGreaterThanOrEqualTo: treePath)
        .where('owner', isLessThanOrEqualTo: '$treePath~')
        .snapshots()
        .map((list) {
      list.docs
          .map((player) => print("=========> " + player.toString()))
          .toList();
      return list.docs.map((player) => Player.fromFirestore(player)).toList();
    });
  }
}

class PlayerEvaluationsData {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  Stream<PlayerEvaluations> get(Player player, String uid) {
    StreamTransformer<DocumentSnapshot<Map<String, dynamic>>, PlayerEvaluations>
        streamTransformer = StreamTransformer.fromHandlers(
      handleData: (data, sink) {
        PlayerEvaluations.fromFirestore(data);
      },
    );

    return _db
        .collection('players')
        .doc(player.uid)
        .collection('evaluations')
        .doc(uid)
        .snapshots()
        .transform(streamTransformer);
  }

  Future<PlayerEvaluations> post(Map<String, dynamic> data) async {
    DocumentReference doc = await _db.collection('players').add(data);
    return doc.get().then((value) =>
        Globals.models[PlayerEvaluations](value) as PlayerEvaluations);
  }
}

class PlayerEvaluationData {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  Stream<PlayerEvaluation> get(Player player, String uid) {
    StreamTransformer<DocumentSnapshot<Map<String, dynamic>>, PlayerEvaluation>
        streamTransformer = StreamTransformer.fromHandlers(
      handleData: (data, sink) {
        PlayerEvaluation.fromFirestore(data);
      },
    );

    return _db
        .collection('players')
        .doc(player.uid)
        .collection('evaluations')
        .doc(uid)
        .snapshots()
        .transform(streamTransformer);
  }

  Future<PlayerEvaluation> post(Player player, User user) async {
    String owner = user.firstName! + ' ' + user.lastName!;

    DocumentReference doc = await _db
        .collection('players')
        .doc(player.uid)
        .collection('evaluations')
        .add({
      'categories': [
        {'type': 'mind', 'items': []},
        {'type': 'athletic', 'items': []},
        {'type': 'technic', 'items': []},
        {'type': 'tactic', 'items': []},
      ],
      'createdAt': Timestamp.now(),
      'ownerName': owner,
    });
    return doc.get().then(
        (value) => Globals.models[PlayerEvaluation](value) as PlayerEvaluation);
  }

  Future<void> delete(Player player, String uid) {
    return _db
        .collection('players')
        .doc(player.uid)
        .collection('evaluations')
        .doc(uid)
        .delete();
  }

  Future<void> upsert(Map<String, dynamic> data,
      PlayerEvaluation playerEvaluation, String playerUid) async {
    Document ref = Document(
        path: 'players/$playerUid/evaluations/${playerEvaluation.uid}');
    return ref._db!
        .doc('players/$playerUid/evaluations/${playerEvaluation.uid}')
        .set(data, SetOptions(merge: true));
  }

  Stream<List<PlayerEvaluation>> list(String playerUid) {
    return _db.collection('players/$playerUid/evaluations').snapshots().map(
        (list) =>
            list.docs.map((pe) => PlayerEvaluation.fromFirestore(pe)).toList());
  }
}

class NoteData {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final String? uidPlayer;

  NoteData({this.uidPlayer});

  Future<Note> post(Map<String, dynamic> data) async {
    DocumentReference doc =
        await _db.collection('players/$uidPlayer/notes').add(data);
    return doc.get().then((value) => Globals.models[Note](value) as Note);
  }

  Future<void> delete(String uid) {
    return _db.collection('players/$uidPlayer/notes').doc(uid).delete();
  }

  Future<void> upsert(Map data, Note note) async {
    Document ref = Document(path: 'players/$uidPlayer/notes/${note.uid}');
    return ref.upsert(data);
  }

  Stream<List<Note>> list(String treePath) {
    return _db
        .collection('players/$uidPlayer/notes')
        .where('owner', isGreaterThanOrEqualTo: treePath)
        .where('owner', isLessThanOrEqualTo: '$treePath~')
        .snapshots()
        .map((list) =>
            list.docs.map((note) => Note.fromFirestore(note)).toList());
  }
}

// class AudioData {
//   final Firestore _db = Firestore.instance;
//   final String uidPlayer;

//   AudioData({this.uidPlayer});

//   Future<Audio> post(Map data) async {
//     DocumentReference doc =
//         await _db.collection('players/$uidPlayer/audios').add(data);
//     return doc.get().then((value) => Globals.models[Audio](value) as Audio);
//   }

//   Future<void> upsert(Map data, Audio audio) async {
//     Document ref = Document(path: 'players/$uidPlayer/audios/${audio.uid}');
//     return ref.upsert(data);
//   }

//   Stream<List<Audio>> list(String treePath) {
//     return _db
//         .collection('players/$uidPlayer/audios')
//         .where('owner', isGreaterThanOrEqualTo: treePath)
//         .where('owner', isLessThanOrEqualTo: '$treePath~')
//         .snapshots()
//         .map((list) =>
//             list.documents.map((audio) => Audio.fromFirestore(audio)).toList());
//   }
// }

class WishlistData {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final auth.FirebaseAuth _auth = auth.FirebaseAuth.instance;

  Future<Wishlist> post(Map<String, dynamic> data) async {
    DocumentReference doc = await _db.collection('wishlists').add(data);
    return doc.get().then((v) => Globals.models[Wishlist](v) as Wishlist);
  }

  Future<void> delete(Wishlist wishlist) {
    return _db.collection('wishlists').doc(wishlist.uid).delete();
  }

  Stream<List<Wishlist>> list() {
    return _auth.authStateChanges().switchMap((user) {
      if (user != null) {
        return _db
            .collection('wishlists')
            .where('user', isEqualTo: user.uid)
            .snapshots()
            .map((list) => list.docs
                .map((whishlist) => Wishlist.fromFirestore(whishlist))
                .toList());
      } else {
        return Stream<List<Wishlist>>.value([]);
      }
    });
  }
}
