import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'dart:io';

class AuthenticationService {
  final FirebaseAuth? _auth = FirebaseAuth.instance;
  final String defaultLocale = Platform.localeName;

  User? get getUser => _auth!.currentUser;

  // Firebase user a realtime stream
  Stream<User?> get user => _auth!.authStateChanges();

  Future loginWithEmail({
    @required String? email,
    @required String? password,
  }) async {
    UserCredential authResult = await _auth!
        .signInWithEmailAndPassword(email: email!, password: password!);

    if (authResult.user == null)
      throw new PlatformException(code: 'LOGIN_FAILED');
  }

  Future<void> signOut() async {
    return _auth!.signOut();
  }

  Future resetPassword(String email) async {
    _auth!.setLanguageCode(defaultLocale);
    await _auth!.sendPasswordResetEmail(email: email);
  }
}
