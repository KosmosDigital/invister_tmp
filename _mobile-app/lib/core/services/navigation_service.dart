import 'package:flutter/widgets.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
    return navigatorKey.currentState!
        .pushNamed(routeName, arguments: arguments);
  }

  // replace the current route by the next route in the stack
  Future<dynamic> replaceAndNavigateTo(String routeName, {dynamic arguments}) {
    return navigatorKey.currentState!
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  // clears the stack then pushes a new route, there's no coming back now
  Future<dynamic> clearAndNavigateTo(String routeName, {dynamic arguments}) {
    return navigatorKey.currentState!
        .pushNamedAndRemoveUntil(routeName, (r) => false, arguments: arguments);
  }

  // clears the stack then pushes a new route, there's no coming back now
  Future<dynamic> clearUntilAndNavigateTo(
      String routeName, RoutePredicate predicate,
      {dynamic arguments}) {
    return navigatorKey.currentState!
        .pushNamedAndRemoveUntil(routeName, predicate, arguments: arguments);
  }

  void goBack() {
    return navigatorKey.currentState!.pop();
  }
}
