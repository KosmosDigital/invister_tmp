import 'package:invister/core/i18n/errorHandler_service.i18n.dart';

class ErrorHandlerService {
  String toFriendlyMessage(error) {
    switch (error.code) {

      // login & signup
      case "ERROR_INVALID_EMAIL":
        return "Your email address appears to be malformed.".i18n;
      case "ERROR_WRONG_PASSWORD":
        return "Your password is wrong.".i18n;
      case "ERROR_USER_NOT_FOUND":
        return "User with this email doesn't exist.".i18n;
      case "ERROR_USER_DISABLED":
        return "User with this email has been disabled.".i18n;
      case "ERROR_TOO_MANY_REQUESTS":
        return "Too many requests. Try again later.".i18n;
      case "ERROR_OPERATION_NOT_ALLOWED":
        return "Signing in with Email and Password is not enabled.".i18n;
      case "ERROR_EMAIL_ALREADY_IN_USE":
        return "Your email address appears to be already used.".i18n;
      case "ERROR_WEAK_PASSWORD":
        return "Your password must contain at least 6 characters.".i18n;
      case "SIGN_UP_FAILED":
        return "Sign up failed.".i18n;
      case "LOGIN_FAILED":
        return "Login failed.".i18n;
      case "ERROR_UNKNOW_USER":
        return "Could not retrieve user.".i18n;

      // network error
      case "ERROR_NETWORK_REQUEST_FAILED":
        return "A network error (such as timeout, interrupted connection or unreachable host) has occurred."
            .i18n;

      // enum
      case "ENUM_NOT_FOUND":
        return "This enum type does not exist.".i18n;

      // recording
      case "NO_RECORDING_PERMISSION":
        return "No recording permission given.".i18n;

      // default
      default:
        return "Unknown error.".i18n;
    }
  }
}
